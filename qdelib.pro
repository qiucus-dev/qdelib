TEMPLATE = lib
CONFIG += staticlib
QT *= core xml gui
QMAKE_CXXFLAGS += -std=c++17 -w

TARGET = qdelib

CONFIG   += console
CONFIG   += depend_includepath
CONFIG   += object_parallel_to_source
DEFINES  += QT_DEPRECATED_WARNINGS



include($$PWD/dataentity/dataentity.pri)

EXT_DIR = $$PWD/../haxscamper-misc/cpp

include($$EXT_DIR/halgorithm.pri)
include($$EXT_DIR/hdebugmacro.pri)
include($$EXT_DIR/hsupport.pri)

