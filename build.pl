#! /usr/bin/env perl
use lib '../../';

use strict;
use warnings;
use Term::ANSIColor;
use File::Copy;
use Switch;
use Getopt::Long;
use Pod::Usage;
use File::Path qw(make_path remove_tree);
use Cwd;
use v5.20;

use scripts::common qw(print_error print_status_small print_status_big print_warning print_status);

use constant false => 0;
use constant true  => 1;

my $script_dir = getcwd;
my $interaction_nonstopmode = true;
my $clean_build;
my $run_quiet;
my $run_silent;
my $dry_run;


use feature qw(signatures);
no warnings qw(experimental::signatures);




sub file_copy ($source, $destination) {
    if (defined $dry_run) {
        print_status_small("   cp $source $destination");
        return true;
    } else {
        return copy($source, $destination);
    }
}

sub change_dir ($target_dir) {
    if(defined $dry_run) {
        print_status_small("   cd $target_dir");
    } else {
        chdir $target_dir;
    }
}

sub prompt_Yn($prompt_message) {
    if ( defined $interaction_nonstopmode ) {
        return false;
    }

    do {
        say "$prompt_message  \033[32m[\033[0my/n\033[32m] \033[0m";
        my $input = <STDIN>;
        chomp $input;

        if ( $input =~ /(yes)|y/i ) {
            return true;
        }
        elsif ( $input =~ /(no)|(not)|n/i ) {
            return false;
        }
    } while (true);
}

sub ignore_errors_or_die($any_errors) {
    if ($any_errors) {
        if ( prompt_Yn("Errors encountered while running. Continue?") ) {
            return false;
        }
        else {
            die "die";
        }
    }
    else {
        return false;
    }
}

sub system_call ( $system_command, $command_name ) {
    print_status("Running $command_name");
    if ( defined $dry_run ) {
        print_status_small("   $system_command");
        return false;
    }

    unless ( system($system_command) ) {
        print_status("Finished running $command_name");
        return false;
    }
    else {
        print_error("Errors encountered while running $command_name");
        return true;
    }

}

sub ensure_build_directory() {
    my $current_dir = getcwd;

    if ( defined $clean_build ) {
        print_status("Performing clean build. Removing build directory");
        if ( -e "build" && -d "build" ) {
            print_status("Build directory is located at $current_dir/build");
            remove_tree("build");
            print_status("Removed build directory");
        }
    }

    if ( -e "build" && -d "build" ) {
        print_status("Build directory exists");
    }
    else {
        print_error("Build directory does not exist. Creating new one");
        mkdir "build";
    }
}

sub build_qdelib_library() {
    print_status_big("Building qdelib library");
    change_dir $script_dir;

    ensure_build_directory();

    change_dir "build";

    my $any_errors = false;

    $any_errors = system_call( "qmake ../qdelib.pro", "make" );
    $any_errors = ignore_errors_or_die($any_errors);

    if ( defined $run_quiet ) {
        $any_errors = system_call( "make -j10", "make" );
    }
    else {
        $any_errors = system_call( "make -j10 --quiet", "make" );
    }

    $any_errors = ignore_errors_or_die($any_errors);

    change_dir "..";

    print_status_big("Finished building library");
}

sub build_swig_python_bindings() {
    print_status_big("Building SWIG python bindings");
    change_dir $script_dir;

    change_dir "bindings";
    print_status("In bindings directory");
    print_status_small("Copying swig files to python directory");

    file_copy( "QDELib.cpp", "python" )
      or die "Failed to file_copy module source file $!";
    file_copy( "QDELib.hpp", "python" )
      or die "Failed to file_copy module header file $!";
    file_copy( "QDELib.swg", "python" ) or die "Failed to file_copy module swig file $!";

    change_dir "python";

    print_status("In python directory");

    my $any_errors = false;

    $any_errors =
      system_call( "swig -c++ -python -modern -py3 QDELib.swg", "swig" );
    $any_errors = ignore_errors_or_die($any_errors);

    ensure_build_directory();

    change_dir "build";

    $any_errors = system_call( "cmake -DCMAKE_BUILD_TYPE=Debug ..", "cmake" );
    $any_errors = ignore_errors_or_die($any_errors);

    if ( defined $run_quiet ) {
        $any_errors = system_call( "make -j10", "make" );
    }
    else {
        $any_errors = system_call( "make -j10 --quiet", "make" );
    }

    $any_errors = ignore_errors_or_die($any_errors);

    print_status("Finished building module");

    file_copy( "_QDELib.so", ".." ) or die "Failed to file_copy shared library. $!";

    change_dir "..";


    print_status("Running test script");
    system("chmod +x test.py");
    system_call("./test.py", "test");
    print_status("Finished running test script");


    file_copy( "_QDELib.so", "../../scripts/python" )
      or die "Failed to file_copy shared library" . " to scripts directory $!";

    file_copy( "QDELib.py", "../../scripts/python" )
      or die "Failed to file_copy shared library" . " to scripts directory $!";

    print_status("Updated module files in script directories");
    print_status_big("Finished building SWIG python bindings");
}

sub build_swig_perl_bindings() {
    print_status_big("Building SWIG perl bindings");
    change_dir $script_dir;

    change_dir "bindings";
    print_status("In bindings directory");
    print_status_small("Copying swig files to python directory");

    file_copy( "QDELib.cpp", "perl" )
      or die "Failed to file_copy module source file $!";
    file_copy( "QDELib.hpp", "perl" )
      or die "Failed to file_copy module header file $!";
    file_copy( "QDELib.swg", "perl" ) or die "Failed to file_copy module swig file $!";

    change_dir "perl";

    print_status("In perl directory");

    my $any_errors = false;

    $any_errors = system_call( "swig -c++ -perl QDELib.swg", "swig" );
    $any_errors = ignore_errors_or_die($any_errors);

    ensure_build_directory();

    change_dir "build";

    $any_errors = system_call( "cmake ..", "cmake" );
    $any_errors = ignore_errors_or_die($any_errors);

    # TODO Implement --silent option
    # TODO Implement --quiet option
    $any_errors = system_call( "make -j10", "make" );
    $any_errors = ignore_errors_or_die($any_errors);

    print_status("Finished building module");

    file_copy( "QDELib.so", ".." ) or die "Failed to file_copy shared library. $!";

    change_dir "..";

    print_status("Running test script");
    system("chmod +x test.pl");
    system_call("./test.pl", "test");
    print_status("Finished running test script");

    file_copy( "QDELib.so", "../../scripts/perl" )
      or die "Failed to file_copy shared librar" . " to script directory $!";
    file_copy( "QDELib.pm", "../../scripts/perl" )
      or die "Failed to file_copy perl module" . " to script directory $!";

    print_status("Updated module files in script directories");
    print_status_big("Finished building SWIG perl bindings");
}

sub build_cpp_tests() {
    print_status_big("Starting cpp test build");

    change_dir $script_dir;
    change_dir "tests";

    ensure_build_directory();

    change_dir "build";

    my $any_errors = false;

    $any_errors = system_call( "cmake ..", "cmake" );
    $any_errors = ignore_errors_or_die($any_errors);

    if ( defined $run_quiet ) {
        $any_errors = system_call( "make -j10", "make" );
    }
    else {
        $any_errors = system_call( "make -j10 --quiet", "make" );
    }

    print_status_big("Finished cpp test build");
}

sub run_cpp_tests() {
    change_dir $script_dir;
    change_dir "tests";

    system("./build/main");
}

my $help;
my $run_all;
my $build_library;
my $build_python_bindings;
my $build_perl_bindings;
my $build_tests_cpp;
my $run_tests_cpp;
my $build_list;
my $global_test;

GetOptions(
    "clean-build"     => \$clean_build,
    "help"            => \$help,
    "dry-run"         => \$dry_run,
    "build-all"       => \$run_all,
    "build-library"   => \$build_library,
    "build-python"    => \$build_python_bindings,
    "build-perl"      => \$build_perl_bindings,
    "build-tests-cpp" => \$build_tests_cpp,
    "run-tests-cpp"   => \$run_tests_cpp,
    "nonstopmode"     => \$interaction_nonstopmode,
    "quiet"           => \$run_quiet,
    "silent"          => \$run_silent,
    "build=s"         => \$build_list,
    "global-test"     => \$global_test,
) or pod2usage( -verbose => 1 ) && exit 0;

if ( defined $help ) {
    pod2usage( -verbose => 1 ) && exit 0;
}

if (defined $global_test) {
    $run_all = true;
    $run_tests_cpp = true;
    $clean_build = true;
}

if ( defined $build_list ) {
    my @build_targets = split( ",", $build_list );
    foreach my $target (@build_targets) {
        switch ($target) {
            case "python"    { $build_python_bindings = true; }
            case "perl"      { $build_perl_bindings = true; }
            case "all"       { $run_all               = true; }
            case "library"   { $build_library         = true; }
            case "tests-cpp" { $build_tests_cpp       = true; }
        }
    }
}

unless ( defined $build_library
    or defined $build_python_bindings
    or defined $build_tests_cpp
    or defined $build_perl_bindings )
{
    $run_all = true;
} else {
    undef $run_all;
}

if(defined $dry_run) {
    print_warning("Performing dry run. No actual build will be performed");
}

if ( defined $run_all or defined $build_library ) {
    build_qdelib_library();
}

if ( defined $run_all or defined $build_python_bindings ) {
    build_swig_python_bindings();
}

if ( defined $run_all or defined $build_perl_bindings ) {
    build_swig_perl_bindings();
}

if ( defined $run_all or defined $build_tests_cpp ) {
    build_cpp_tests();
}

if ( defined $run_tests_cpp ) {
    run_cpp_tests();
}

# TODO qmake temporary file directory
# TODO Clean build remove all *.o *_moc.cpp files in the directory

=head1 NAME

build.pl

=head1 SYNOPSIS

build.pl [OPTIONS]

=head1 OPTIONS

 --help -h         Print help message
 --clean-build -c  Clean build. Remove all build files and start build.
 --dry-run -d      Run script but do not perform any building. Used for
                   debug.
 --build-all       Build library and every single module
 --build-library   Build library only. Can be combined with other flags
                   for choosing build targets
                   default options in all prompts.
 --quiet           Reduce number of messages. Do not print recipes and
                   warnings in make.
 --silent          Print *nothing*. Redirect and stdio of external
                   programs to /dev/null while calling.
 --build=          List of comma-separater build targets. Target names
                   are identical to ones in --build-* options. I.e.
                   --build=library,python is identical to --build-library
                   --build-python,.

                   Supported options are: python, library, tests-cpp, perl,
                                          all

                   For running tests use `run-tests-*`
 --run-tests-cpp   Run current version of the test executable. For running
                   relevant version (If you made any changes and want to
                   see the effect) use `--build-tests-cpp` or --build=tests-cpp
                   options.

 --global-test     Perform clean build of all modules and library, run all
                   tests for all modules and library

=cut
