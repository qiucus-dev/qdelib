#include "support.hpp"
#include <qdelib/qsevent.hpp>
#include <qdelib/qstodo.hpp>

TEST(QSEvent, Write) {
    qde::QSEvent event;
    QString      path = getTestDir() + "/qsevent/write_test";
    ASSERT_TRUE(event.saveFile(path, true));
}


TEST(QSEvent, Read) {
    qde::QSEvent event;
    QString      path = getTestDir() + "/qsevent/write_test";
    ASSERT_TRUE(event.openFile(path, true));
}


TEST(QSEvent, WriteReadEqual) {
    qde::QSEvent event_1;
    qde::QSEvent event_2;
    setRandomValues(event_1);

    event_1.appendTodo(std::make_unique<qde::QSTodo>());
    event_1.addEventTag("urgent");
    event_1.setRepeatTimes(10);

    QString path = getTestDir() + "/qsevent/read_write_test";

    ASSERT_TRUE(event_1.saveFile(path, true));
    ASSERT_TRUE(event_2.openFile(path, true));

    ASSERT_TRUE(event_1 == event_2);
}
