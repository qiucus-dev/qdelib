#include "support.hpp"
#include <qdelib/qstodo.hpp>

using qde::QSTodo;
using qde::QSTodoUptr;

TEST(QSTodo, Write) {
    QSTodo  todo;
    QString path = getTestDir() + "/qstodo/write_test";
    ASSERT_TRUE(todo.saveFile(path, true));
}


TEST(QSTodo, Read) {
    QSTodo  todo;
    QString path = getTestDir() + "/qstodo/write_test";
    ASSERT_TRUE(todo.openFile(path, true));
}


TEST(QSTodo, WriteReadEqual) {
    QSTodo todo_1;
    QSTodo todo_2;
    setRandomValues(todo_1);

    todo_1.addTodoTags(spt::getRandomBase64List());
    todo_1.setPriority(QSTodo::Priority::Blocking);
    todo_1.setStatus(QSTodo::Status::Completed);
    todo_1.setEstimatedTime(QSTodo::EstimatedTime::_3_6_hours);
    todo_1.setNecessity(QSTodo::Necessity::Useful);

    QString path = getTestDir() + "/qstodo/read_write_test";

    todo_1.saveFile(path, true);
    todo_2.openFile(path, true);


    ASSERT_TRUE(todo_1.getDeadline() == todo_2.getDeadline());
    ASSERT_TRUE(todo_1 == todo_2);
}


TEST(QSTodo, NestedOneLevelReadWriteEqual) {
    QSTodo     todo_1;
    QSTodoUptr nested = std::make_unique<QSTodo>();
    setRandomValues(*nested);
    todo_1.setExactName("Exact name: todo 1");
    todo_1.setMetaName("Todo 1");

    todo_1.appendNestedTodo(std::move(nested));

    QString path = getTestDir()
                   + "/qstodo/nested_one_level_read_write_equal";

    todo_1.saveFile(path, true);

    QSTodo todo_2;
    todo_2.openFile(path, true);

    ASSERT_TRUE(todo_1.getNestedTodoRef().size() == 1);
    ASSERT_TRUE(todo_2.getNestedTodoRef().size() == 1);

    ASSERT_TRUE(
        (*todo_2.getNestedTodoRef()[0])
        == (*todo_1.getNestedTodoRef()[0]));
}

TEST(QSTodo, CreateCopyEqual) {
    QSTodoUptr todo1 = std::make_unique<QSTodo>();
    setRandomValues(*todo1);

    QSTodoUptr copy = todo1->createUptrCopy();

    ASSERT_TRUE(*todo1 == *copy);
}

TEST(QSTodo, DependencySerialization) {
    QString prefix = getTestDir() + "/qstodo/dependency_serialization";

    {
        QSTodo todo1;
        setRandomValues(todo1);
        QSTodo todo2;
        setRandomValues(todo2);

        todo1.addDependency(todo2);
        todo1.addDependency(todo2);
        todo1.addDependency(todo2);
        todo1.addDependency(todo2);

        INFO << "====";
        todo1.printToDebug();

        ASSERT_TRUE(todo1.dependsOn(todo2));

        todo1.saveFile(prefix + "todo_1", true);
        todo2.saveFile(prefix + "todo_2", true);
    }

    {
        QSTodo todo1;
        QSTodo todo2;

        todo1.openFile(prefix + "todo_1", true);
        todo2.openFile(prefix + "todo_2", true);


        INFO << "====";
        todo1.printToDebug();

        ASSERT_TRUE(todo1.dependsOn(todo2));
    }
}
