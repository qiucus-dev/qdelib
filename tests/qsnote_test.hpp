#include "support.hpp"
#include <qdelib/qsnote.hpp>


TEST(QSNote, Write) {
    qde::QSNote note;
    QString      path = getTestDir() + "/qsnote/write_test";
    ASSERT_TRUE(note.saveFile(path, true));
}


TEST(QSNote, Read) {
    qde::QSNote note;
    QString      path = getTestDir() + "/qsnote/write_test";
    ASSERT_TRUE(note.openFile(path, true));
}


TEST(QSNote, WriteReadEqual) {
    qde::QSNote note_1;
    qde::QSNote note_2;
    setRandomValues(note_1);

    note_1.setNoteTags(spt::getRandomBase64List());
    note_1.setNoteText(spt::getRandomBase64(1000));

    QString path = getTestDir() + "/qsnote/read_write_test";

    ASSERT_TRUE(note_1.saveFile(path, true));
    ASSERT_TRUE(note_2.openFile(path, true));
    ASSERT_TRUE(note_2.saveFile(path + "2", true));
    ASSERT_TRUE(note_1 == note_2);
}
