#include "support.hpp"
#include <qdelib/qsimage.hpp>


TEST(QSImage, Write) {
  qde::QSImage image;
  QString      path = getTestDir() + "/qsimage/write_test";
  ASSERT_TRUE(image.saveFile(path, true));
}


TEST(QSImage, Read) {
  qde::QSImage image;
  QString      path = getTestDir() + "/qsimage/write_test";
  ASSERT_TRUE(image.openFile(path, true));
}


TEST(QSImage, WriteReadEqual) {
  qde::QSImage image_1;
  qde::QSImage image_2;

  QString path = getTestDir() + "/qsimage/read_write_test";

  ASSERT_TRUE(image_1.saveFile(path, true));
  ASSERT_TRUE(image_2.openFile(path, true));
  ASSERT_TRUE(image_1 == image_2);
}
