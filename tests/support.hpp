#pragma once

#include <QDir>
#include <QFileInfo>
#include <gtest/gtest.h>
#include <halgorithm/all.hpp>
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>
#include <qdelib/dataentity.hpp>

QString getTestDir() {
    return QFileInfo(__FILE__).absoluteDir().path() + "/tmp";
}


void setRandomValues(qde::DataEntity& obj) {
    QStringList tags;
    for (int i = 0; i < 3; ++i) {
        tags.push_back("#" + spt::getRandomBase64List(3, 5).join("##"));
    }

    obj.setMetaTags(std::move(tags));
    obj.addMetaTag("singletag");
    obj.setMetaDescription(spt::getRandomBase64());
    obj.setMetaName(spt::getRandomBase64());
    obj.setMetaNote(spt::getRandomBase64());
    obj.setMetaSource(spt::getRandomBase64());
    // for (size_t i = 0; i < 3; ++i) {
    //     obj.setCustomData({spt::getRandomBase64(),
    //                        spt::getRandomBase64List().join("\n")});
    // }
}
