#include "support.hpp"
#include <qdelib/qsnote.hpp>
#include <qdelib/qstable.hpp>


TEST(QSTable, Write) {
    qde::QSTable table;
    QString       path = getTestDir() + "/qstable/write_test";
    ASSERT_TRUE(table.saveFile(path, true));
}


TEST(QSTable, Read) {
    qde::QSTable table;
    QString       path = getTestDir() + "/qstable/write_test";
    ASSERT_TRUE(table.openFile(path, true));
}


TEST(QSTable, WriteReadEqual) {
    qde::QSTable table_1;
    qde::QSTable table_2;

    QString path = getTestDir() + "/qstable/read_write_test";

    ASSERT_TRUE(table_1.saveFile(path, true));
    ASSERT_TRUE(table_2.openFile(path, true));
    ASSERT_TRUE(table_1 == table_2);
}

TEST(QSTable, WriteReadEqualElements) {
    qde::QSTable table_1;
    qde::QSTable table_2;
    qde::QSNoteUptr note = std::make_unique<qde::QSNote>();

    note->setNoteText(spt::getRandomBase64(1000));

    table_1.resize(1,1,1);
    table_1[0][0][0] = std::move(note);

    QString path = getTestDir() + "/qstable/read_write_test";

    ASSERT_TRUE(table_1.saveFile(path, true));
    ASSERT_TRUE(table_2.openFile(path, true));
    ASSERT_TRUE(table_1[0][0][0] == table_2[0][0][0]);
}
