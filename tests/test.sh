#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

./clean.sh
mkdir -p build
cd build
cmake ..
make -j12 
./main

