#!/usr/bin/env bash
mkdir build
cd build
cmake  -DCMAKE_BUILD_TYPE=Debug .. 1> cmake_messages 2> cmake_errors
make -j10 --quiet 1> make_messages 2> make_errors
cp --update main ..
cd ..
