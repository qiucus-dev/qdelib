cmake_minimum_required(VERSION 3.6)

find_package(GTest REQUIRED)
find_package(Qt5Core CONFIG REQUIRED)
find_package(Qt5Xml CONFIG REQUIRED)
find_package(Qt5Gui CONFIG REQUIRED)


add_executable(
    main
    main.cpp
    )


set_property(
    TARGET
    main
    PROPERTY
    CXX_STANDARD
    17)

target_include_directories(
    main PUBLIC
    ${Qt5Core_INCLUDE_DIRS}
    ${Qt5Xml_INCLUDE_DIRS}
    ${Qt5Gui_INCLUDE_DIRS}
    ${GTEST_INCLUDE_DIRS}
    # QDELib
    ${CMAKE_CURRENT_SOURCE_DIR}/../include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../haxscamper-misc/cpp/halgorithm/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../haxscamper-misc/cpp/hdebugmacro/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../haxscamper-misc/cpp/hsupport/include
    )

target_link_libraries(
    main
    ${Qt5Core_LIBRARIES}
    ${Qt5Xml_LIBRARIES}
    ${Qt5Gui_LIBRARIES}
    ${GTEST_LIBRARIES}
    # QDELib
    ${CMAKE_CURRENT_SOURCE_DIR}/../lib/libqdelib.a
    ${CMAKE_CURRENT_SOURCE_DIR}/../../haxscamper-misc/cpp/hsupport/lib/libhsupport.a
    )
