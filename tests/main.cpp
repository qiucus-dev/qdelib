#include "qsevent_test.hpp"
#include "qslink_test.hpp"
#include "qsnote_test.hpp"
#include "qstodo_test.hpp"
// #include "qsimage_test.hpp"
// #include "qstable_test.hpp"

int main(int argc, char** argv) {
    //   DEBUG_LOGGER_USE_GTEST_PREFIX;
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
