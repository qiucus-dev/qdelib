#pragma once

#include "qsnote.hpp"


namespace qde {
    /*!
     * \brief Convert text of QSNote between different formats
     */
class QSNote_Converter
{
  public:
    void    convert(QSNote* note, QSNote::NoteType targetType) const;
    QString convert(
        QString          sourceText,
        QSNote::NoteType sourceType,
        QSNote::NoteType targetType) const;

    QString getFilePath() const;
    void    setFilePath(const QString& value);

  private:
    QString filePath;
};
} // namespace qde


