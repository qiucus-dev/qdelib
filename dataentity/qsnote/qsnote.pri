HEADERS *= \
    $$PWD/qsnote.hpp \
    $$PWD/qsnote_xmlreader.hpp \
    $$PWD/qsnote_converter.hpp \
    $$PWD/qsnote_converter.hpp

SOURCES *= \
    $$PWD/qsnote.cpp \
    $$PWD/qsnote_xmlreader.cpp \
    $$PWD/qsnote_converter.cpp \
    $$PWD/qsnote_converter.cpp \
    $$PWD/qsnote_file.cpp \
    $$PWD/qsnote_misc.cpp
