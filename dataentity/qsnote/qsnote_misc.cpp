/*!
 * \file qsnote_misc.cpp
 * \brief Getters/setters for QSNote
 */


//=== Sibling  ===//
#include "qsnote.hpp"
#include <hdebugmacro/all.hpp>


//  ////////////////////////////////////


namespace qde {
QString QSNote::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {
    QString res = DataItem::toDebugString(
        indentationLevel, printEverything, recurseLevel);

    static const int justify = xmlTags.base.debugStringSize;

    res += noteText.isNull() && !printEverything
               ? ""
               : "Note text:\n" + noteText + "\n";

    res += noteTags.isEmpty() && !printEverything
               ? ""
               : dbg::justify("Note tags:", justify)
                     + QString::fromStdString(noteTags.toDebugString())
                     + "\n";

    return res;
}

bool QSNote::operator==(const QSNote& other) const {
    return baseMetadata == other.baseMetadata //
           && noteTags == other.noteTags      //
           && noteType == other.noteType      //
           && noteText == other.noteText;
}


spt::TagSet QSNote::getNoteTags() {
    return noteTags;
}


void QSNote::setNoteTags(const spt::TagSet& value) {
    noteTags = value;
}


void QSNote::addNoteTag(const spt::Tag& tag) {
    noteTags.addTag(tag);
}


QSNote::NoteType QSNote::getNoteType() const {
    return noteType;
}


QString QSNote::getNoteTypeString() const {
    return spt::enum_to_string<NoteType>(noteType);
}


void QSNote::setNoteType(const NoteType& value) {
    noteType = value;
}


void QSNote::setNoteType(QString value) {
    noteType = spt::enum_from_string<NoteType>(value);
}


QString QSNote::getNoteText() const {
    return noteText;
}


void QSNote::setNoteText(const QString& value) {
    noteText = value;
}
} // namespace qde

namespace spt {
template <>
QString enum_to_string<qde::QSNote::NoteType>(qde::QSNote::NoteType opt) {
    using Enum = qde::QSNote::NoteType;
    switch (opt) {
        case Enum::Asciidoctor: return "Asciidoctor";
        case Enum::RichText: return "RichText";
        case Enum::Qozim: return "Qozim";
        case Enum::LaTeX: return "LaTeX";
    }
}

template <>
qde::QSNote::NoteType enum_from_string<qde::QSNote::NoteType>(
    QString str) {
    using Enum = qde::QSNote::NoteType;
    Enum res;

    if (str == "Asciidoctor") {
        res = Enum::Asciidoctor;
    } else if (str == "RichText") {
        res = Enum::RichText;
    } else if (str == "Qozim") {
        res = Enum::Qozim;
    } else if (str == "LaTeX") {
        res = Enum::LaTeX;
    } else {
        throw std::invalid_argument("Undefined note type");
    }

    return res;
}
} // namespace spt
