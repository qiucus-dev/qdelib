#pragma once

class QXmlStreamReader;

namespace qde {
class QSNote;
namespace xml {
    void readQSNoteXML(
        QSNote*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags = nullptr);
} // namespace xml
} // namespace qde
