


/*! \file qsnote_file.cpp
 *  \brief File-related functions for QSNote class
 */

#include "qsnote.hpp"
#include "qsnote_xmlreader.hpp"
#include <hsupport/xml.hpp>


namespace qde {
QDomElement QSNote::toXML(QDomDocument& document) const {
    QDomElement XML_root = document.createElement(xmlTags.qsnote.rootName);

    XML_root.appendChild(getBaseMetadataRef().toXML(document));

    QDomElement XML_note_body = document.createElement(
        xmlTags.qsnote.content);

    XML_note_body.appendChild(spt::textElement(
        document, xmlTags.qsnote.type, getNoteTypeString()));

    XML_note_body.appendChild(
        spt::textElement(document, xmlTags.qsnote.text, noteText, true));

    XML_note_body.appendChild(
        noteTags.toXML(document, xmlTags.qsnote.tags));

    XML_root.appendChild(XML_note_body);


    return XML_root;
}


void qde::QSNote::fromXML(QXmlStreamReader* xmlStream) {
    xml::readQSNoteXML(this, xmlStream);
}

QString QSNote::getExtension(bool prependComma) const {
    if (prependComma) {
        return "." + xmlTags.qsnote.extension;
    } else {
        return xmlTags.qsnote.extension;
    }
}
} // namespace qde
