#pragma once


/*!
 * \file qsnote.hpp
 * \brief
 */

//===    Qt    ===//
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QString>
#include <QTextDocument>


//===    STL   ===//
#include <memory>


//=== Sibling  ===//
#include "../base/dataitem.hpp"
#include "qsnote_xmlreader.hpp"
#include <hsupport/tagset.hpp>


//  ////////////////////////////////////


namespace qde {

/*!
 * \brief QSNote class represents regular note similar to one that you
 * can find in services like Evernote.
 */
class QSNote : public DataItem
{
    friend void xml::readQSNoteXML(
        QSNote*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags);

  public:
    struct QSNoteXMLTags : BaseMetadata::XMLTags {
        struct {
            QString rootName  = "qsnote-root";
            QString content   = "qsnote-content";
            QString tags      = "note-tags";
            QString type      = "note-type";
            QString text      = "note-text";
            QString extension = "qsnote";
        } qsnote;
    };


    /// Types of note markup
    enum class NoteType
    {
        LaTeX,
        /*!
         * \brief Qt rich text see <a
         * href="http://doc.qt.io/archives/qt-4.8/richtext-html-subset.html">
         * supported html subset</a>
         */
        RichText,
        /*!
         * \brief Asciidoctor markup. See <a
         * href="https://asciidoctor.org/">asciidoctor</a>
         */
        Asciidoctor,
        /*!
         * Placeholder
         */
        Qozim
    };

    bool        operator==(const QSNote& other) const;
    static bool isXMLRoot(const QString& elementName);
    spt::TagSet getNoteTags();
    void        setNoteTags(const spt::TagSet& value);
    void        addNoteTag(const spt::Tag& tag);
    NoteType    getNoteType() const;
    QString     getNoteTypeString() const;
    void        setNoteType(const NoteType& value);
    void        setNoteType(QString value);
    QString     getNoteText() const;
    void        setNoteText(const QString& value);
    void        convertTo(NoteType targetType);

  private:
    spt::TagSet noteTags; ///< Tags associated with this note
    /// Type of the note (e.g. LaTeX, adoc, RichText)
    NoteType noteType = NoteType::Asciidoctor;
    QString  noteText; ///< Body of the note

    /// Helper variable to keep use of tags consistent across all xml
    /// readers and writers
    static QSNoteXMLTags xmlTags;

    //#= DataEntity interface
  public:
    QDomElement toXML(QDomDocument& document) const override;
    void        fromXML(QXmlStreamReader* xmlStream) override;
    QString     getExtension(bool prependComma = false) const override;
    QString     toDebugString(
            size_t indentationLevel = 0,
            bool   printEverything  = false,
            int    recurseLevel     = -1) override;
};

using QSNoteUptr = std::unique_ptr<QSNote>;
} // namespace qde


namespace spt {
template <>
QString enum_to_string<qde::QSNote::NoteType>(qde::QSNote::NoteType opt);

template <>
qde::QSNote::NoteType enum_from_string<qde::QSNote::NoteType>(QString str);
} // namespace spt

Q_DECLARE_METATYPE(qde::QSNote*)
