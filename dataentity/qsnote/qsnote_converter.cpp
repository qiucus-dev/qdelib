


#include "qsnote_converter.hpp"

namespace qde {
void QSNote_Converter::convert(
    qde::QSNote*          note,
    qde::QSNote::NoteType targetType) const {
    qde::QSNote::NoteType sourceType = note->getNoteType();
    note->setNoteText(
        convert(note->getNoteText(), sourceType, targetType));
}

QString QSNote_Converter::convert(
    QString                sourceText,
    qde::QSNote::NoteType sourceType,
    qde::QSNote::NoteType targetType) const {
    if (sourceType == targetType) {
        return sourceText;
    }

    return sourceText;
}


QString QSNote_Converter::getFilePath() const {
    return filePath;
}


void QSNote_Converter::setFilePath(const QString& value) {
    filePath = value;
}
} // namespace qde
