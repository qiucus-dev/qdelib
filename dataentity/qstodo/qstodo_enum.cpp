#include "qstodo.hpp"

namespace spt {

template <>
QString enum_to_string<qde::QSTodo::Priority>(qde::QSTodo::Priority opt) {
    using Enum = qde::QSTodo::Priority;

    switch (opt) {
        case Enum::NoPriority: return "NoPriority";
        case Enum::Low: return "Low";
        case Enum::Medium: return "Medium";
        case Enum::High: return "High";
        case Enum::Critical: return "Critical";
        case Enum::Blocking: return "Blocking";
        case Enum::Undefined: return "Undefined";
        case Enum::CurrentlyWorking: return "CurrentlyWorking";
        case Enum::Organization: return "Organization";
        case Enum::Later: return "Later";
    }
}

template <>
qde::QSTodo::Priority enum_from_string<qde::QSTodo::Priority>(
    QString str) {
    using Enum = qde::QSTodo::Priority;
    Enum res;

    if (str == "Undefined") {
        res = Enum::Undefined;
    } else if (str == "NoPriority") {
        res = Enum::NoPriority;
    } else if (str == "Low") {
        res = Enum::Low;
    } else if (str == "Medium") {
        res = Enum::Medium;
    } else if (str == "High") {
        res = Enum::High;
    } else if (str == "Critical") {
        res = Enum::Critical;
    } else if (str == "Blocking") {
        res = Enum::Blocking;
    } else if (str == "CurrentlyWorking") {
        res = Enum::CurrentlyWorking;
    } else if (str == "Organization") {
        res = Enum::Organization;
    } else if (str == "Later") {
        res = Enum::Later;
    } else {
        throw std::invalid_argument("Undefined note type");
    }

    return res;
}

//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::Status>(qde::QSTodo::Status opt) {
    using Enum = qde::QSTodo::Status;
    switch (opt) {
        case Enum::Cancelled: return "Cancelled";
        case Enum::Completed: return "Completed";
        case Enum::InProgress: return "InProgress";
        case Enum::NeedsAction: return "NeedsAction";
        case Enum::Postponed: return "Postponed";
        case Enum::Deleted: return "Deleted";
        case Enum::Nuked: return "Nuked";
        case Enum::Stalled: return "Stalled";
        case Enum::NoStatus: return "NoStatus";
        case Enum::Undefined: return "Undefined";
    }
}

template <>
qde::QSTodo::Status enum_from_string<qde::QSTodo::Status>(QString str) {
    using Enum = qde::QSTodo::Status;
    Enum res;

    if (str == "NoStatus") {
        res = Enum::NoStatus;
    } else if (str == "Undefined") {
        res = Enum::Undefined;
    } else if (str == "NeedsAction") {
        res = Enum::NeedsAction;
    } else if (str == "InProgress") {
        res = Enum::InProgress;
    } else if (str == "Completed") {
        res = Enum::Completed;
    } else if (str == "Cancelled") {
        res = Enum::Cancelled;
    } else if (str == "Postponed") {
        res = Enum::Postponed;
    } else if (str == "Deleted") {
        res = Enum::Deleted;
    } else if (str == "Stalled") {
        res = Enum::Stalled;
    } else if (str == "Nuked") {
        res = Enum::Nuked;
    } else {
        throw std::invalid_argument("Undefined note type");
    }


    return res;
}

//  ///////////////////////////////////////////////////////////////////////


template <>
QString enum_to_string<qde::QSTodo::EstimatedTime>(
    qde::QSTodo::EstimatedTime opt) {
    using Enum = qde::QSTodo::EstimatedTime;
    switch (opt) {
        case Enum::_5_10_min: return "_5_10_min";
        case Enum::_10_30_min: return "_10_30_min";
        case Enum::_1_2_hours: return "_1_2_hours";
        case Enum::_3_6_hours: return "_3_6_hours";
        case Enum::_1_2_days: return "_1_2_days";
        case Enum::_5_6_days: return "_5_6_days";
        case Enum::_1_3_weeks: return "_1_3_weeks";
        case Enum::_1_2_months: return "_1_2_months";
        case Enum::_5_6_months: return "_5_6_months";
        case Enum::_forever: return "_forever";
        case Enum::NoEstimate: return "NoEstimate";
        case Enum::Undefined: return "Undefined";
    }
}


template <>
qde::QSTodo::EstimatedTime enum_from_string<qde::QSTodo::EstimatedTime>(
    QString str) {
    using Enum = qde::QSTodo::EstimatedTime;
    Enum res;

    if (str == "NoEstimate") {
        res = Enum::NoEstimate;
    } else if (str == "Undefined") {
        res = Enum::Undefined;
    } else if (str == "_5_10_min") {
        res = Enum::_5_10_min;
    } else if (str == "_10_30_min") {
        res = Enum::_10_30_min;
    } else if (str == "_1_2_hours") {
        res = Enum::_1_2_hours;
    } else if (str == "_3_6_hours") {
        res = Enum::_3_6_hours;
    } else if (str == "_1_2_days") {
        res = Enum::_1_2_days;
    } else if (str == "_5_6_days") {
        res = Enum::_5_6_days;
    } else if (str == "_1_3_weeks") {
        res = Enum::_1_3_weeks;
    } else if (str == "_1_2_months") {
        res = Enum::_1_2_months;
    } else if (str == "_5_6_months") {
        res = Enum::_5_6_months;
    } else if (str == "_forever") {
        res = Enum::_forever;
    } else {
        throw std::invalid_argument("Undefined note type");
    }

    return res;
}


//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::Necessity>(
    qde::QSTodo::Necessity opt) {
    using Enum = qde::QSTodo::Necessity;
    switch (opt) {
        case Enum::NoNecessity: return "NoNecessity";
        case Enum::Undefined: return "Undefined";
        case Enum::Useless: return "Useless";
        case Enum::Useful: return "Useful";
        case Enum::VeryUseful: return "VeryUseful";
        case Enum::Important: return "Important";
        case Enum::VeryImportant: return "VeryImportant";
    }
}

template <>
qde::QSTodo::Necessity enum_from_string<qde::QSTodo::Necessity>(
    QString str) {
    using Enum = qde::QSTodo::Necessity;
    Enum res;

    if (str == "NoNecessity") {
        res = Enum::NoNecessity;
    } else if (str == "Undefined") {
        res = Enum::Undefined;
    } else if (str == "Useless") {
        res = Enum::Useless;
    } else if (str == "Useful") {
        res = Enum::Useful;
    } else if (str == "VeryUseful") {
        res = Enum::VeryUseful;
    } else if (str == "Important") {
        res = Enum::Important;
    } else if (str == "VeryImportant") {
        res = Enum::VeryImportant;
    } else {
        throw std::invalid_argument("Undefined note type");
    }

    return res;
}

//  ///////////////////////////////////////////////////////////////////////


template <>
QString enum_to_string<qde::QSTodo::CompletionLikelihood>(
    qde::QSTodo::CompletionLikelihood opt) {
    using Enum = qde::QSTodo::CompletionLikelihood;
    switch (opt) {
        case Enum::NoLikelihood: return "NoLikelihood";
        case Enum::Undefined: return "Undefined";
        case Enum::Guaranteed: return "Guaranteed";
        case Enum::High: return "High";
        case Enum::Medium: return "Medium";
        case Enum::Small: return "Small";
    }
}


template <>
qde::QSTodo::CompletionLikelihood enum_from_string<
    qde::QSTodo::CompletionLikelihood>(QString str) {
    using Enum = qde::QSTodo::CompletionLikelihood;
    Enum res;
    if (str == "NoLikelihood") {
        res = Enum::NoLikelihood;
    } else if (str == "Undefined") {
        res = Enum::Undefined;
    } else if (str == "Guaranteed") {
        res = Enum::Guaranteed;
    } else if (str == "High") {
        res = Enum::High;
    } else if (str == "Medium") {
        res = Enum::Medium;
    } else if (str == "Small") {
        res = Enum::Small;
    } else {
        throw std::invalid_argument("Undefined note type");
    }

    return res;
}

} // namespace spt
