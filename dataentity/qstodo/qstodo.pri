HEADERS *= \
    $$PWD/qstodo.hpp \
    $$PWD/qstodo_xmlreader.hpp

SOURCES *= \
    $$PWD/qstodo.cpp \
    $$PWD/qstodo_xmlreader.cpp \
    $$PWD/qstodo_misc.cpp \
    $$PWD/qstodo_enum.cpp


