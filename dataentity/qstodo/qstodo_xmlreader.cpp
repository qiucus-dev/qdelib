#include "qstodo_xmlreader.hpp"
#include "qstodo.hpp"
#include <halgorithm/all.hpp>
#include <hdebugmacro/all.hpp>


namespace qde {
namespace xml {
    static void readNestedTodos(
        QSTodo*                target,
        QXmlStreamReader*      xmlStream,
        QSTodo::QSTodoXMLTags* tags) {
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->qstodo.nestedTodo) {
                QSTodoUptr nested = std::make_unique<QSTodo>();
                xmlStream->readNextStartElement();
                nested->fromXML(xmlStream);
                target->appendNestedTodo(std::move(nested));
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }

    static void readQSTodoContent(
        QSTodo*                target,
        QXmlStreamReader*      xmlStream,
        QSTodo::QSTodoXMLTags* tags) {
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->qstodo.deadline) {
                target->setDeadline(
                    QDateTime::fromString(xmlStream->readElementText()));

                // Tags
            } else if (xmlStream->name() == tags->qstodo.tags) {
                // TODO rvalue as argument for function that accepts
                // reference? Check if this is indeed the case and if
                // possible test for correctness
                target->setTodoTags(
                    spt::TagSet::fromXML(xmlStream, tags->qstodo.tags));

                // Nested
            } else if (xmlStream->name() == tags->qstodo.nestedTodos) {
                readNestedTodos(target, xmlStream, tags);

                // Estimated time
            } else if (xmlStream->name() == tags->qstodo.estimate) {
                target->setEstimatedTime(
                    spt::enum_from_string<QSTodo::EstimatedTime>(
                        xmlStream->readElementText()));

                // Priority
            } else if (xmlStream->name() == tags->qstodo.priority) {
                target->setPriority(
                    spt::enum_from_string<QSTodo::Priority>(
                        xmlStream->readElementText()));

                // Necessity
            } else if (xmlStream->name() == tags->qstodo.necessity) {
                target->setNecessity(
                    spt::enum_from_string<QSTodo::Necessity>(
                        xmlStream->readElementText()));

                // Completion likelihood
            } else if (
                xmlStream->name() == tags->qstodo.completionLikelihood) {
                target->setCompletionLikelihood(
                    spt::enum_from_string<QSTodo::CompletionLikelihood>(
                        xmlStream->readElementText()));

                // Status
            } else if (xmlStream->name() == tags->qstodo.status) {
                target->setStatus(spt::enum_from_string<QSTodo::Status>(
                    xmlStream->readElementText()));

                // Dependencies
            } else if (xmlStream->name() == tags->qstodo.dependencies) {
                while (xmlStream->readNextStartElement()) {
                    if (xmlStream->name()
                        == tags->descriptor.descriptorName) {
                        target->addDependency(
                            DataDescriptor::fromXML(xmlStream));
                    } else {
                        xmlStream->skipCurrentElement();
                    }
                }

                // Attachments
            } else if (xmlStream->name() == tags->qstodo.attachments) {
                while (xmlStream->readNextStartElement()) {
                    if (xmlStream->name() == tags->qstodo.attachments) {
                        target->addAttachment(
                            Attachment::fromXML(xmlStream));
                    } else {
                        xmlStream->skipCurrentElement();
                    }
                }

            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }


    void readQSTodoXML(
        QSTodo*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags) {
        if (xmlStream->name().isEmpty()) {
            xmlStream->readNextStartElement();
        }

        QSTodo::QSTodoXMLTags* tags;
        if (_tags == nullptr) {
            tags = &target->xmlTags;
        } else {
            tags = static_cast<QSTodo::QSTodoXMLTags*>(_tags);
        }

        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->base.section) {
                target->getBaseMetadataRef().readXML(xmlStream);
            } else if (xmlStream->name() == tags->qstodo.content) {
                readQSTodoContent(target, xmlStream, tags);
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }

} // namespace xml
} // namespace qde
