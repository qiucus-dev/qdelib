#pragma once

class QXmlStreamReader;

namespace qde {
class QSTodo;
namespace xml {
    void readQSTodoXML(
        QSTodo*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags = nullptr);
} // namespace xml
} // namespace qde
