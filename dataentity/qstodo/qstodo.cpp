/*!
 * \file QSTodo.cpp
 */

//===   STL    ===//
//#include <filesystem>
//#include <system_error>


//=== Internal ===//
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>


//=== Sibling  ===//
#include "qstodo.hpp"
#include "qstodo_xmlreader.hpp"


//  ////////////////////////////////////


namespace qde {
QSTodo::QSTodoXMLTags QSTodo::xmlTags;


bool QSTodo::operator==(const QSTodo& other) const {
    return CMP_DBG(
               DataItem::operator==(static_cast<const DataItem&>(other)))
           && CMP_DBG(deadline == other.deadline)
           && CMP_DBG(todoTags == other.todoTags)
           && CMP_DBG(weight == other.weight)
           && CMP_DBG(estimatedTime == other.estimatedTime)
           && CMP_DBG(
                  necessity == other.necessity
                  && priority == other.priority)
           && CMP_DBG(status == other.status)
           && CMP_DBG(std::equal(
                  timeRanges.begin(),
                  timeRanges.end(),
                  other.timeRanges.begin(),
                  other.timeRanges.end()));

    ;
}

QSTodo::QSTodo() {
    baseMetadata.xmlVersion = spt::Version(2);
}

/*!
 * \brief Create copy of the object. Nested todos are copied recursively,
 * their order is preserved
 * \todo Write test to check whether or not copies are identical
 */
QSTodoUptr QSTodo::createUptrCopy() const {
    QSTodoUptr copy = std::make_unique<QSTodo>();

    copy->setDeadline(this->deadline);
    copy->setTodoTags(this->todoTags);
    copy->setComments(this->comments);
    copy->setWeight(this->weight);
    copy->setEstimatedTime(this->estimatedTime);
    copy->setPriority(this->priority);
    copy->setStatus(this->status);
    copy->setNecessity(this->necessity);
    copy->setCompletionLikelihood(this->completionLikelihood);
    copy->setAttachments(this->attachments);
    copy->setTimeRanges(this->timeRanges);
    copy->setTaskDescription(this->taskDescription);

    copy->baseMetadata = this->baseMetadata;

    for (const QSTodoUptr& nested : nested) {
        copy->appendNestedTodo(nested->createUptrCopy());
    }

    return copy;
}

QDomElement QSTodo::toXML(QDomDocument& document) const {
    QDomElement xmlRoot = document.createElement(xmlTags.qstodo.rootName);
    xmlRoot.appendChild(getBaseMetadataRef().toXML(document));

    QDomElement xmlContent = document.createElement(
        xmlTags.qstodo.content);
    {

        if (!deadline.isNull()) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.deadline,
                deadline.toString(Qt::ISODate)));
        }

        if (todoTags.size() != 0) {
            xmlContent.appendChild(
                todoTags.toXML(document, xmlTags.qstodo.tags));
        }

        if (priority != Priority::NoPriority) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.priority,
                spt::enum_to_string<Priority>(priority)));
        }


        if (status != Status::NoStatus) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.status,
                spt::enum_to_string<Status>(status)));
        }

        if (estimatedTime != EstimatedTime::NoEstimate) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.estimate,
                spt::enum_to_string<EstimatedTime>(estimatedTime)));
        }

        if (necessity != Necessity::NoNecessity) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.necessity,
                spt::enum_to_string<Necessity>(necessity)));
        }

        if (completionLikelihood != CompletionLikelihood::NoLikelihood) {
            xmlContent.appendChild(spt::textElement(
                document,
                xmlTags.qstodo.completionLikelihood,
                spt::enum_to_string<CompletionLikelihood>(
                    completionLikelihood)));
        }

        if (weight != 0) {
            xmlContent.appendChild(spt::textElement(
                document, xmlTags.qstodo.weight, QString::number(weight)));
        }

        if (comments.size() != 0) {
            xmlContent.appendChild(spt::list_to_xml(
                comments,
                document,
                [](const QString& string) -> QString { return string; },
                xmlTags.qstodo.comments,
                xmlTags.qstodo.comment));
        }

        if (!taskDescription.isEmpty()) {
            xmlContent.appendChild(spt::textElement(
                document, xmlTags.qstodo.description, taskDescription));
        }

        if (nested.size() != 0) {
            xmlContent.appendChild(spt::list_to_xml(
                nested,
                document,
                [&document](const QSTodoUptr& uptr) -> QDomElement {
                    return uptr->toXML(document);
                },
                xmlTags.qstodo.nestedTodos,
                xmlTags.qstodo.nestedTodo));
        }

        if (attachments.size() != 0) {
            QDomElement attachmentList = document.createElement(
                xmlTags.qstodo.attachments);

            for (const Attachment& attachment : attachments) {
                attachmentList.appendChild(attachment.toXML(document));
            }

            xmlContent.appendChild(attachmentList);
        }

        if (dependencies.size() != 0) {
            QDomElement depsList = document.createElement(
                xmlTags.qstodo.dependencies);

            for (const DataDescriptor& deps : dependencies) {
                depsList.appendChild(deps.toXML(document));
            }

            xmlContent.appendChild(depsList);
        }


        if (timeRanges.size() != 0) {
            xmlContent.appendChild(spt::list_to_xml(
                timeRanges,
                document,
                [&document](const spt::DateTimeRange& range)
                    -> QDomElement { return range.toXML(document); },
                xmlTags.qstodo.timeRanges));
        }
    }

    xmlRoot.appendChild(xmlContent);

    return xmlRoot;
}


void QSTodo::fromXML(QXmlStreamReader* xmlStream) {
    xml::readQSTodoXML(this, xmlStream);
}


QString QSTodo::getExtension(bool prependDot) const {
    if (prependDot) {
        return "." + xmlTags.qstodo.extension;
    } else {
        return xmlTags.qstodo.extension;
    }
}

QString QSTodo::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {

    const uint justify = xmlTags.base.debugStringSize;
    const uint indent  = xmlTags.base.nestedIndentSize * indentationLevel;

    QString res = QString(indent, ' ') + "==== QSTodo::toDebugString:\n";
    res += deadline.isNull() && !printEverything
               ? ""
               : dbg::debug_one_line_value(
                     indent,
                     "Deadline",
                     justify,
                     deadline.toString(Qt::ISODate));

    if (dependencies.size() != 0 || printEverything) {
        res += dbg::debug_one_line_value(
            indent, "Dependencies", justify, "");
        for (const DataDescriptor& descr : dependencies) {
            res += dbg::debug_one_line_value(
                indent + 2,
                "UUid",
                justify,
                descr.uuid.toString(QUuid::WithoutBraces));
        }
    }

    res += dbg::debug_one_line_value(
        indent,
        "Necessity",
        justify,
        spt::enum_to_string<Necessity>(necessity));

    res += dbg::debug_one_line_value(
        indent,
        "Priority",
        justify,
        spt::enum_to_string<Priority>(priority));

    res += dbg::debug_one_line_value(
        indent, "Status", justify, spt::enum_to_string<Status>(status));

    res += dbg::debug_one_line_value(
        indent,
        "EstimatedTime",
        justify,
        spt::enum_to_string<EstimatedTime>(estimatedTime));

    res += dbg::debug_one_line_value(
        indent, "Weight", justify, QString::number(weight));


    res += DataItem::toDebugString(indentationLevel + 1, printEverything);

    if (recurseLevel != 0) {
        res += QString(indent, ' ') + "---- Nested todos:\n";
        for (QSTodoUptr& todo : nested) {
            res += QString(' ', indent)
                   + todo->toDebugString(
                         indentationLevel + 1,
                         printEverything,
                         recurseLevel - 1);
        }
    }


    return res;
}
} // namespace qde
