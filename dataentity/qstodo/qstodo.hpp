#pragma once


/*!
 * \file qslink.hpp
 */

//===    Qt    ===//
#include <QDateTime>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QTextDocument>
#include <QtCore>
#include <QtXml>


//===    STL   ===//
#include <utility>
#include <variant>
#include <vector>


//=== Internal ===//
#include <hsupport/datetime.hpp>
#include <hsupport/single_select.hpp>


//=== Sibling  ===//
#include "../base/dataitem.hpp"
#include "qstodo_xmlreader.hpp"


//  ////////////////////////////////////


namespace qde {
class QSTodo;
using QSTodoUptr = std::unique_ptr<QSTodo>;


/*!
 * \brief The QSTodo class represents todo (task) together with nested
 * sub-tasks
 */
class QSTodo : public DataItem
{
    friend void xml::readQSTodoXML(
        QSTodo*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags);

  public:
    /// Current completion status of the task
    enum class Status
    {
        Undefined,   ///< Undefined status
        NoStatus,    ///< Task does not have any status
        NeedsAction, ///< Things need to be done
        InProgress,  ///< Task is currenly being completed
        Completed,   ///< Task has been completed
        Cancelled,   ///< Task has been cancelled
        Postponed,   ///< Task deadline has been moved into the future
        Deleted,     ///< task has been deleted
        Nuked,       ///< nuked
        Stalled,     ///< Stalled
    };

    /// Priority of the task
    enum class Priority : size_t
    {
        Undefined,  ///< Undefined priority
        NoPriority, ///< Task does not have any priority
        Low,        ///< Low importance
        Medium,     ///< Medium importance
        High,       ///< High importance
        Critical,   ///< Critical importance
        Blocking,   ///< No other tasks can be completed before this
        /*!
         * Priority does not matter now because task is currently being
         * completed
         */
        CurrentlyWorking,
        /*!
         * Priority does not matter now because task involves
         * organizational activities and can be performed during period of
         * time of any length
         */
        Organization,
        /// Priority does not matter now because this will be done after
        /// undefined preiod of time
        Later
    };

    /// Estimated amount of time that will be spent to complete this task
    enum class EstimatedTime : size_t
    {
        Undefined,   ///< Undefined amount of time
        NoEstimate,  ///< No times estimates
        _5_10_min,   ///< 5 to 10 minutes
        _10_30_min,  ///< 10 to 30 minutes
        _1_2_hours,  ///< 1 to 2 hours
        _3_6_hours,  ///< 3 to 6 hours
        _1_2_days,   ///< 1 to 2 days
        _5_6_days,   ///< 5 to 6 days
        _1_3_weeks,  ///< 1 to 3 weeks
        _1_2_months, ///< 1 to 2 months
        _5_6_months, ///< 5 to 6 months
        _forever     ///< Unlimited amount of time
    };

    /// How necessary given task is
    enum class Necessity
    {
        Undefined,   ///< Undefined necessity
        NoNecessity, ///< Necessity not assigned
        Useless,     ///< Task's completion is meaningless
        Useful,      ///< Task's completion has some value
        VeryUseful,  ///< Task's completion has a lot of value
        Important,   ///< Task's completion is enforced by external actions
        VeryImportant, ///< Task's completion is strictly enforced by
                       ///< external actions
    };

    /// Chance of this task actually being completed in time (without
    /// moving it in the future)
    enum class CompletionLikelihood
    {
        Undefined,    ///< Undefined completion likelihood
        NoLikelihood, ///< Likelihood is not defined
        Small,        ///< Small completion likelihood
        Medium,       ///< Medium completion likelihood
        High,         ///< High completion likelihood
        Guaranteed    ///< Task is guaranteed to be completed
    };

  public:
    struct QSTodoXMLTags : BaseMetadata::XMLTags {
        struct {
            /// Name of the root element
            QString rootName    = "qstodo-root";
            QString extension   = "qstodo";         ///< File extension
            QString content     = "qstodo-content"; ///< Start data section
            QString deadline    = "deadline";       ///< Deadline DateTime
            QString tags        = "tags";
            QString status      = "status";
            QString priority    = "priority";
            QString weight      = "weight";
            QString comments    = "comments";
            QString comment     = "comment";
            QString nestedTodos = "nested-todos";
            QString nestedTodo  = "nested-todo";
            QString attachments = "attachments";
            QString attachment  = "attachment";
            QString timeRanges  = "time-ranges";
            QString estimate    = "estimate-time";
            QString necessity   = "necessity";
            QString description = "description";
            QString dependencies = "dependencies";

            QString completionLikelihood = "completion-likelihood";
            QString estimatePrecise      = "estimate-time-precise";
            QString estimateSelector     = "estimate-time-selector";
        } qstodo;
    };

    virtual ~QSTodo() override = default;
    bool operator==(const QSTodo& other) const;

    //# Content variables
  private:
    static QSTodoXMLTags xmlTags;
    /// Child tasks
    std::vector<QSTodoUptr> nested;

    /// Deadline for a given task
    QDateTime deadline;

    /// Tags associated with given todo
    spt::TagSet todoTags;

    /// Comments associated with given todo
    std::vector<QString> comments;

    /// Weight of given task. Does not have range boundaries. If value is
    /// equal to `0` (default value) it is not serialized
    int weight = 0;

    /// Estimated time to finish given task. If value is equal to
    /// EstimatedTime::NoNecessity (default value) it is not serialized
    EstimatedTime estimatedTime = EstimatedTime::NoEstimate;

    /// Priority of a given task. If value is equal to
    /// Priority::NoNecessity (default value) it is not serialized
    Priority priority = Priority::NoPriority;

    /// Completion status of a given task. If value is equal to
    /// Necessity::NoNecessity (default value) it is not serialized
    Status status = Status::NoStatus;

    /// Necessity of a given task. If value is equal to
    /// Necessity::NoNecessity (default value) it is not serialized
    Necessity necessity = Necessity::NoNecessity;

    /// How many changes of this task being completed in time. If value is
    /// equal to CompletionLikelihood::NoNecessity (default value) it is
    /// not serialized
    CompletionLikelihood completionLikelihood = CompletionLikelihood::
        NoLikelihood;

    /// List of dependencies
    std::vector<DataDescriptor> dependencies;

    /// Attachment files
    std::vector<Attachment> attachments;

    /// Time ranges during which this task will be completed
    spt::DateTimeRanges timeRanges;

    /// Textual description of the task
    QString taskDescription;


    //#= Special memeber functions
  public:
    QSTodo();

    //# Member access funcitons
  public:
    //#== Time-related
    QDateTime getDeadline() const;
    void      setDeadline(const QDateTime& value);
    void      setEstimatedTime(EstimatedTime _estimatedTime);
    void      setTimeRanges(spt::DateTimeRanges ranges);

    //#== Property-related
    CompletionLikelihood getCompletionLikelihood() const;
    void setCompletionLikelihood(const CompletionLikelihood& value);
    void setPriority(Priority _priority);
    void setStatus(Status _status);
    void setNecessity(Necessity _necessity);
    void setWeight(int _weight);


    //#== Content-related
    //#=== Tags
    spt::TagSet getTodoTags() const;
    void        setTodoTags(const spt::TagSet& value);
    void        addTodoTag(spt::Tag newTag);
    void        addTodoTags(spt::TagSet newTags);
    //#=== Externals
    void setAttachments(const std::vector<Attachment>& _attachments);
    void addAttachment(const Attachment& attachment);
    //#==== Nested todos
    void                     appendNestedTodo(QSTodoUptr nestedTodo);
    std::vector<QSTodoUptr>& getNestedTodoRef();
    bool                     insertNestedTodo(
                            QSTodoUptr nestedTodo,
                            QSTodo*    insert,
                            bool       insertAfter = true);
    //#==== Dependencies
    bool dependsOn(const QSTodo& todo) const;
    void addDependency(const QSTodo& todo);
    void addDependency(const DataDescriptor& todo);
    void removeDependecy(const QSTodo& todo);

    ///#=== Internals
    QString getTaskDescription() const;
    void    setTaskDescription(QString description);
    void    setComments(const std::vector<QString>& _comments);


    QSTodoUptr createUptrCopy() const;


    //#= DataObject interface
  public:
    QDomElement toXML(QDomDocument& document) const override;
    void        fromXML(QXmlStreamReader* xmlStream) override;
    QString     getExtension(bool prependDot = false) const override;
    QString     toDebugString(
            size_t indentationLevel = 0,
            bool   printEverything  = false,
            int    recurseLevel     = -1) override;
};
} // namespace qde


namespace spt {
template <>
qde::QSTodo::Priority enum_from_string<qde::QSTodo::Priority>(QString str);

template <>
QString enum_to_string<qde::QSTodo::Priority>(qde::QSTodo::Priority opt);

//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::Status>(qde::QSTodo::Status opt);

template <>
qde::QSTodo::Status enum_from_string<qde::QSTodo::Status>(QString str);


//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::EstimatedTime>(
    qde::QSTodo::EstimatedTime opt);

template <>
qde::QSTodo::EstimatedTime enum_from_string<qde::QSTodo::EstimatedTime>(
    QString str);

//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::Necessity>(qde::QSTodo::Necessity opt);

template <>
qde::QSTodo::Necessity enum_from_string<qde::QSTodo::Necessity>(
    QString str);


//  ///////////////////////////////////////////////////////////////////////

template <>
QString enum_to_string<qde::QSTodo::CompletionLikelihood>(
    qde::QSTodo::CompletionLikelihood opt);

template <>
qde::QSTodo::CompletionLikelihood enum_from_string<
    qde::QSTodo::CompletionLikelihood>(QString str);


} // namespace spt


Q_DECLARE_METATYPE(qde::QSTodo*)
