SOURCES +=  \
    $$PWD/ndimarray.cpp \
    $$PWD/slice.cpp

HEADERS += \
    $$PWD/ndimarray.hpp \
    $$PWD/slice.hpp \
    $$PWD/_include.hpp

INCLUDEPATH += $$PWD
