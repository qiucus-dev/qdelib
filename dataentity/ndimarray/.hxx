#pragma once

#include <boost/multi_array.hpp>
#include <boost/variant.hpp>
#include <vector>
#include <algorithm>
#include <exception>
#include <QVariant>

class DataObject
{
public:
  DataObject() {}
};

typedef boost::variant<int, QVariant, DataObject*> Cell;
typedef boost::multi_array<Cell, 1> Vector;
typedef boost::multi_array<Cell, 2> Matrix;
typedef boost::multi_array<Cell, 3> Tensor;
// Techincally, matrix and vector is 2nd and 1nd order tensors
typedef boost::variant<int, Vector, Matrix, Tensor> Array;
typedef boost::multi_array_types::index_range Range;
typedef Vector::array_view<1>::type VectorView;
typedef Matrix::array_view<2>::type MatrixView;
typedef Tensor::array_view<3>::type TensorView;
typedef boost::variant<int, MatrixView, VectorView, TensorView> ArrayView;


