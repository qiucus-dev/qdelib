


#include "ndimarray.hpp"


namespace dent {
NDimArray::NDimArray() {
    dimensions = 3;
}


ContainerItem* NDimArray::at(Position _pos, bool check) {
    dimension_assert(_pos);
    assert(check || indexAllowed(_pos));
    Position position = toRealPos(_pos);
    switch (dimensions) {
        case 3: return &(array[position.X()][position.Y()][position.Z()]);
        case 2: return &(array[position.X()][position.Y()][0]);
        case 1: return &(array[position.X()][0][0]);
        case 0: assert(false); break;
    }


    return &(array[position.X()][position.Y()][position.Z()]);
}


ContainerItem* NDimArray::at(int _x, int _y, int _z, bool check) {
    return at(Position(_x, _y, _z), check);
}


Position NDimArray::toRealPos(Position _pos) {
    dimension_assert(_pos);
    auto shape = array.shape();

    auto checker = [&](int dim_index, int value) -> int {
        if (value < 0) {
            return shape[dim_index] - (abs(_pos.X()) % shape[dim_index]);
        } else {
            if (value < shape[dim_index]) {
                return value;
            } else {
                return value % shape[dim_index];
            }
        }
    };


    int real_x = checker(0, _pos.X());
    int real_y = checker(0, _pos.Y());
    int real_z = checker(0, _pos.Z());
    return Position().X(real_x).Y(real_y).Z(real_z);
}


bool NDimArray::indexAllowed(Position _pos) {
    dimension_assert(_pos);
    auto shape   = array.shape();
    bool allowed = false;
    switch (dimensions) {
        case 3:
            allowed
                = (_pos.X() < shape[0] && //
                   _pos.Y() < shape[1] && //
                   _pos.Z() < shape[2]);
            break;
        case 2:
            allowed
                = (_pos.X() < shape[0] && //
                   _pos.Y() < shape[1] && //
                   _pos.Z() == 0);
            break;
        case 1:
            allowed
                = (_pos.X() < shape[0] && //
                   _pos.Y() == 0 &&       //
                   _pos.Z() == 0);
            break;
        case 0:
            allowed
                = (_pos.X() == 0 && //
                   _pos.Y() == 0 && //
                   _pos.Z() == 0);
            break;
        default: allowed = false; break;
    }
    return allowed;
}


void NDimArray::reshape(Slice slice) {
    array.resize(
        boost::extents[slice.sizeX()][slice.sizeY()][slice.sizeZ()]);
}


void NDimArray::reshape(int newX, int newY, int newZ) {
    switch (dimensions) {
        case 3: array.resize(boost::extents[newX][newY][newZ]); break;
        case 2: array.resize(boost::extents[newX][newY][1]); break;
        case 1: array.resize(boost::extents[newX][1][1]); break;
        default: assert(false); break;
    }
}


void NDimArray::reshape(Position _pos) {
    reshape(_pos.X(), _pos.Y(), _pos.Z());
}


Slice NDimArray::shape() {
    auto shape = array.shape();
    return Slice().setX(shape[0]).setY(shape[1]).setZ(shape[2]);
}


unsigned int NDimArray::getDimensions() const {
    return dimensions;
}


void NDimArray::setDimensions(unsigned int value) {
    dimensions = value;
}


void NDimArray::clear() {
    reshape(0, 0, 0);
}


void NDimArray::dimension_assert(Position _pos) {
    switch (dimensions) {
        case 3: break;
        case 2: assert(_pos.Z() == 0); break;
        case 1: assert(_pos.Z() == 0 && _pos.Y() == 0); break;
        case 0:
            assert(_pos.Z() == 0 && _pos.Y() == 0 && _pos.X() == 0);
            break;
    }
}


int Position::X() const {
    return x;
}


Position& Position::X(int value) {
    x = value;
    return *this;
}


int Position::Y() const {
    return y;
}


Position& Position::Y(int value) {
    y = value;
    return *this;
}


int Position::Z() const {
    return z;
}


Position& Position::Z(int value) {
    z = value;
    return *this;
}
} // namespace dent
