

#include "slice.hpp"

Slice::Slice() {}

Slice::Slice(int _x, int _y, int _z) {
    x_final = _x;
    y_final = _y;
    z_final = _z;
}

Slice& Slice::setX(int x_start, int x_end) {
    x_begin = x_start;
    x_final = x_end;
    return *this;
}

Slice& Slice::setY(int y_start, int y_end) {
    y_begin = y_start;
    y_final = y_end;
    return *this;
}

Slice& Slice::setZ(int z_start, int z_end) {
    z_begin = z_start;
    z_final = z_end;
    return *this;
}

Slice& Slice::setX(int x_end) {
    x_begin = 0;
    x_final = x_end;
    assert(x_end >= 0);
    return *this;
}

Slice& Slice::setY(int y_end) {
    y_begin = 0;
    y_final = y_end;
    assert(y_end >= 0);
    return *this;
}

Slice& Slice::setZ(int z_end) {
    z_begin = 0;
    z_final = z_end;
    assert(z_end >= 0);
    return *this;
}

int Slice::sizeX() {
    int x = x_final - x_begin;
    return (x == 0) ? 1 : x;
}

int Slice::sizeY() {
    int y = y_final - y_begin;
    return (y == 0) ? 1 : y;
}

int Slice::sizeZ() {
    int z = z_final - z_begin;
    return (z == 0) ? 1 : z;
}

Range Slice::rangeX() { return Range(x_begin, x_final); }

Range Slice::rangeY() { return Range(y_begin, y_final); }

Range Slice::rangeZ() { return Range(z_begin, z_final); }

int Slice::dimCount() {
    int res = 0;
    res += (x_begin == x_final) ? 0 : 1;
    res += (y_begin == y_final) ? 0 : 1;
    res += (z_begin == z_final) ? 0 : 1;
    return res;
}

bool Slice::isPoint() {
    return (x_begin == x_final && y_begin == y_final && z_begin == z_final);
}

int Slice::getxBegin() const { return x_begin; }

void Slice::setxBegin(int value) { x_begin = value; }

int Slice::getxFinal() const { return x_final; }

void Slice::setxFinal(int value) { x_final = value; }

int Slice::getyBegin() const { return y_begin; }

void Slice::setyBegin(int value) { y_begin = value; }

int Slice::getyFinal() const { return y_final; }

void Slice::setyFinal(int value) { y_final = value; }

int Slice::getzBegin() const { return z_begin; }

void Slice::setzBegin(int value) { z_begin = value; }

int Slice::getzFinal() const { return z_final; }

void Slice::setzFinal(int value) { z_final = value; }
