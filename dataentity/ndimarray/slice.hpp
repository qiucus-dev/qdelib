#pragma once

#include <cassert>
#include <QString>
#include "_include.hpp"

class Slice
{
  public:
    Slice();
    Slice(int _x, int _y = 0, int _z = 0);
    Slice& setX(int x_start, int x_end);
    Slice& setY(int y_start, int y_end);
    Slice& setZ(int z_start, int z_end);
    Slice& setX(int x_end);
    Slice& setY(int y_end);
    Slice& setZ(int z_end);
    int    sizeX();
    int    sizeY();
    int    sizeZ();
    Range  rangeX();
    Range  rangeY();
    Range  rangeZ();
    int    dimCount();

    bool isPoint();

    int  getxBegin() const;
    void setxBegin(int value);

    int  getxFinal() const;
    void setxFinal(int value);

    int  getyBegin() const;
    void setyBegin(int value);

    int  getyFinal() const;
    void setyFinal(int value);

    int  getzBegin() const;
    void setzBegin(int value);

    int  getzFinal() const;
    void setzFinal(int value);

  private:
    int x_begin = 0;
    int x_final = 0;

    int y_begin = 0;
    int y_final = 0;

    int z_begin = 0;
    int z_final = 0;
};

