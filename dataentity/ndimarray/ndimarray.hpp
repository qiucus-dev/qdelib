#pragma once

#include "_include.hpp"
#include "slice.hpp"
#include <QDebug>
#include <tuple>
#include <gsl/pointers>

namespace dent {

class Position
{
  public:
    Position() = default;

    Position(Slice _slice) {
        x = _slice.getxFinal();
        y = _slice.getyFinal();
        z = _slice.getzFinal();
    }


    Position(int _x, int _y, int _z) {
        x = _x;
        y = _y;
        z = _z;
    }


    int       X() const;
    Position& X(int value);
    int       Y() const;
    Position& Y(int value);
    int       Z() const;
    Position& Z(int value);

  private:
    int x;
    int y;
    int z;
};

class NDimView
{
  public:
    NDimView() {
    }
};

class NDimArray
{
  public:
    NDimArray();
    explicit NDimArray(NDimView* view);

    NDimArray      slice(Slice slice);
    ContainerItem* at(Position _pos, bool check = true);
    ContainerItem* at(int _x, int _y = 0, int _z = 0, bool check = true);
    Position       toRealPos(Position _pos);
    void           reshape(Slice slice);
    void           reshape(int newX, int newY = 0, int newZ = 0);
    void           reshape(Position _pos);
    Slice          shape();

    unsigned int getDimensions() const;
    void         setDimensions(unsigned int value);
    void         clear();

  private:
    friend class NDimView;
    Tensor       array;
    unsigned int dimensions;
    void         dimension_assert(Position _pos);

    bool indexAllowed(Position _pos);
    bool indexAllowed(Slice slice);
};
} // namespace dent
