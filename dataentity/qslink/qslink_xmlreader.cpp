#include "qslink_xmlreader.hpp"
#include "qslink.hpp"
#include <QXmlStreamReader>

namespace qde {
namespace xml {

    static void readQSLinkContent(
        QSLink*                target,
        QXmlStreamReader*      xmlStream,
        QSLink::QSLinkXMLTags* tags) {

        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->qslink.url) {
                target->setURL(QUrl(xmlStream->readElementText()));
            } else if (xmlStream->name() == tags->qslink.linkTags) {
                target->setLinkTags(spt::TagSet::fromXML(
                    xmlStream, tags->qslink.linkTags));
            } else if (xmlStream->name() == tags->qslink.contentText) {
                target->setContentText(xmlStream->readElementText());
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }


    void readQSLinkXML(
        QSLink*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags) {

        if (xmlStream->name().isEmpty()) {
            xmlStream->readNextStartElement();
        }

        QSLink::QSLinkXMLTags* tags;
        if (_tags == nullptr) {
            tags = &target->xmlTags;
        } else {
            tags = static_cast<QSLink::QSLinkXMLTags*>(_tags);
        }

        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->base.section) {
                target->getBaseMetadataRef().readXML(xmlStream);
            } else if (xmlStream->name() == tags->qslink.content) {
                readQSLinkContent(target, xmlStream, tags);
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }
} // namespace xml
} // namespace qde
