/*!
 * \file qslink.cpp
 */

//===   STL    ===//
//#include <filesystem>
//#include <system_error>


//=== Internal ===//
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>
#include <hsupport/xml.hpp>


//=== Sibling  ===//
#include "qslink.hpp"
#include "qslink_xmlreader.hpp"


//  ////////////////////////////////////


namespace qde {
QSLink::QSLinkXMLTags QSLink::xmlTags;

void QSLink::setURL(QUrl url) {
    linkUrl = url;
}


void QSLink::setURL(QString url) {
    linkUrl = QUrl(url);
}


QUrl QSLink::getURL() const {
    return linkUrl;
}


void QSLink::setContentText(QString text) {
    contentText = text;
}


QString QSLink::getContentText() const {
    return contentText;
}


void QSLink::setLinkTags(spt::TagSet tags) {
    urlTags = std::move(tags);
}


spt::TagSet QSLink::getLinkTags() {
    return urlTags;
}

spt::TagSet* QSLink::getLinkTagsPtr() {
    return &urlTags;
}


void QSLink::addLinkTag(spt::Tag tag) {
    urlTags.addTag(tag);
}


QDomElement QSLink::toXML(QDomDocument& document) const {
    QDomElement XML_root = document.createElement(xmlTags.qslink.rootName);
    XML_root.appendChild(getBaseMetadataRef().toXML(document));

    QDomElement XML_qslink_content = document.createElement(
        xmlTags.qslink.content);

    XML_qslink_content.appendChild(spt::textElement(
        document, xmlTags.qslink.url, linkUrl.toString()));

    XML_qslink_content.appendChild(spt::textElement(
        document, xmlTags.qslink.contentText, contentText));

    XML_qslink_content.appendChild(
        urlTags.toXML(document, xmlTags.qslink.linkTags));

    XML_root.appendChild(XML_qslink_content);


    return XML_root;
}


void QSLink::fromXML(QXmlStreamReader* xmlStream) {
    xml::readQSLinkXML(this, xmlStream);
}


void QSLink::readFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    QFile file(path);
    if (!file.exists()) {
        //        spt::throw_file_does_not_exist(path);
    } else if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        //        spt::throw_file_cannot_be_opened(path);
    } else {
        QXmlStreamReader xmlStream;
        xmlStream.setDevice(&file);
        this->fromXML(&xmlStream);
    }
}

void QSLink::writeFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    if (!QFile(path).exists()) {
        //        spt::throw_file_does_not_exist(path);
    } else {
        QDomDocument document;
        document.appendChild(this->toXML(document));
        spt::writeDocument(document, path);
    }
}


bool QSLink::saveFile(
    QString path,
    bool    appendExtension,
    bool    autoCreate) {

    if (appendExtension && !path.endsWith(getExtension(true))) {
        path += getExtension(true);
    }

    if (autoCreate && !spt::createFilePath(path)) {
        return false;
    }

    bool success = false;
    try {
        writeFile(path);
        success = true;
    } catch (...) {}

    return success;
}


bool QSLink::openFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    bool success = false;
    try {
        readFile(path);
        success = true;
    } catch (...) {}
    return success;
}


QString QSLink::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {
    QString res = DataItem::toDebugString(
        indentationLevel, printEverything, recurseLevel);

    static const int justify = xmlTags.base.debugStringSize;

    res += urlTags.isEmpty() && !printEverything
               ? ""
               : dbg::justify("Link tags:", justify)
                     + QString::fromStdString(urlTags.toDebugString())
                     + "\n";

    return res;
}


bool QSLink::operator==(const QSLink& other) const {
    return DataItem::operator==(static_cast<const DataItem&>(other))
           && linkUrl == other.linkUrl && contentText == other.contentText
           && urlTags == other.urlTags;
}


QString QSLink::getExtension(bool prependDot) const {
    if (prependDot) {
        return ".qslink";
    } else {
        return "qslink";
    }
}
} // namespace qde
