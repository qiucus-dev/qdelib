#pragma once

class QXmlStreamReader;

namespace qde {
class QSLink;
namespace xml {
    void readQSLinkXML(
        QSLink*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags = nullptr);
} // namespace xml
} // namespace qde
