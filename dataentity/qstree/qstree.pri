HEADERS *= \
    $$PWD/qstree.hpp \
    $$PWD/treeitem.h

SOURCES *= \
    $$PWD/qstree.cpp \
    $$PWD/treeitem.cpp
