#pragma once

#include "../base/datacontainer.hpp"


namespace qde {
class QSTree : public DataContainer
{
  public:
    QSTree();
};
} // namespace qde


Q_DECLARE_METATYPE(qde::QSTree*)

