#pragma once

#include <port.hpp>
#include "mindedge.hpp"

class MindPort : Port
{
  public:
    MindPort();

  private:
    QList<MindEdge*> edges;
};

