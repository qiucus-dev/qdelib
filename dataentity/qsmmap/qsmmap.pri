HEADERS += \
    $$PWD/qsmmap.hpp \
    $$PWD/mindnode.hpp \
    $$PWD/mindedge.hpp \
    $$PWD/mindport.hpp

SOURCES += \
    $$PWD/qsmmap.cpp \
    $$PWD/mindnode.cpp \
    $$PWD/mindedge.cpp \
    $$PWD/mindport.cpp
