#include "mindnode.hpp"

MindNode::MindNode() { data = QVariant(QString("none")); }

QList<Port*> MindNode::getPorts() const { return ports; }

void MindNode::setPorts(const QList<Port*>& value) { ports = value; }

QVariant MindNode::getData() const { return data; }

void MindNode::setData(const QVariant& value) { data = value; }
