#pragma once

#include "../base/dataentity_xmlreader.hpp"
#include "qsimage.hpp"

namespace qde {
namespace xml {
    class QSImage_XMLReader : public DataEntity_XMLReader
    {
      public:
        QSImage_XMLReader(QXmlStreamReader* _xmlStream);
        void setTarget(QSImage* _target);

      private:
        QSImage*                 target = nullptr;
        QSImage::QSImageXMLTags* tags;

        //#= DataEntity_XMLReader interface
      public:
        void readXML() override;
    };
} // namespace xml
} // namespace qde
