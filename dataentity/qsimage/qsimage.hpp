#pragma once

/*!
 * \file qsimage.hpp
 */

//===    Qt    ===//
#include <QImage>

//=== Sibling  ===//
#include "../base/dataitem.hpp"


//  ////////////////////////////////////
namespace qde {
namespace xml {
    class QSImage_XMLReader;
}
} // namespace qde

namespace qde {
/*!
 * \brief The QSImage class represents image with
 * associated tags/description etc.
 */
class QSImage : public DataItem
{
    friend class xml::QSImage_XMLReader;

  public:
    virtual ~QSImage() override = default;

    struct QSImageXMLTags : BaseMetadata::XMLTags {
        struct {
            QString rootName  = "qsimage-root";
            QString content   = "qsimage-content";
            QString tags      = "qsimage-tags";
            QString imageData = "image-data";
            QString extension = "qsimage";
        } qsimage;
    };

  private:
    static QSImageXMLTags xmlTags;
    QImage                image;

    //#= DataEntity interface
  public:
    virtual QDomElement toXML(QDomDocument& document) const override;
    virtual void        fromXML(QXmlStreamReader* xmlStream) override;
    virtual QString     getExtension(bool prependDot) const override;
    virtual QString     toDebugString(
            size_t indentationLevel,
            bool   printEverything,
            int    recurseLevel) override;
};
} // namespace qde
