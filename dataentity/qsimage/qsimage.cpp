#include "qsimage.hpp"

#include <QBuffer>
#include <QByteArray>

#include "qsimage_xmlreader.hpp"


namespace qde {

QSImage::QSImageXMLTags QSImage::xmlTags;

QDomElement QSImage::toXML(QDomDocument& document) const {
    QDomElement root = document.createElement(xmlTags.qsimage.rootName);
    QDomElement metadataXML = baseMetadata.toXML(document);
    QDomElement content = document.createElement(xmlTags.qsimage.content);

    root.appendChild(metadataXML);
    root.appendChild(content);

    {
        QByteArray byteArray;
        QBuffer    buffer(&byteArray);
        image.save(&buffer, "PNG");
        content.appendChild(spt::textElement(
            document,
            xmlTags.qsimage.imageData,
            byteArray.toBase64(),
            true));
    }


    return root;
}

void QSImage::fromXML(QXmlStreamReader* xmlStream) {
    xml::QSImage_XMLReader xmlReader(xmlStream);
}

QString QSImage::getExtension(bool prependDot) const {
    if (prependDot) {
        return "." + xmlTags.qsimage.extension;
    } else {
        return xmlTags.qsimage.extension;
    }
}

QString QSImage::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {
    QString result = DataItem::toDebugString(
        indentationLevel, printEverything, recurseLevel);

    return result;
}
} // namespace qde
