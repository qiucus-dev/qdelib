#pragma once

#include "../base/dataitem.hpp"
#include "../qstodo/qstodo.hpp"
#include "qsevent_xmlreader.hpp"

#include <QColor>
#include <QDateTime>
#include <hsupport/misc.hpp>

namespace qde {
class QSEvent;
using QSEventUptr = std::unique_ptr<QSEvent>;


/*!
 * \brief The QSEvent class represents single calendar event with together
 * with sub-events and associated todos. Event might occuoy multiple time
 * ranges.
 */
class QSEvent : public DataItem
{
    friend void xml::readQSEventXML(
        QSEvent*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags);

  public:
    struct QSEventXMLTags : BaseMetadata::XMLTags {
        struct {
            QString content        = "qsevent-content";
            QString rootName       = "qsevent-root";
            QString extension      = "qsevent";
            QString dateTimeRanges = "datetime-ranges";
            QString nestedEvents   = "nested-events";
            QString nestedTodos    = "nested-todos";
            QString repeatInterval = "repeat-interval";
            QString repeatTimes    = "repeat-times";
            QString eventStart     = "event-start";
            QString eventType      = "event-type";
            QString eventTags      = "event-tags";
        } qsevent;
    };

    enum class EventType
    {
        /// Regular event
        Default,
        /// Event occupies whole day for each day in date time ranges
        AllDay,
        /// Event might occupy any period of time but happens in the
        /// background
        Background,
    };

    virtual ~QSEvent() override = default;
    bool operator==(const QSEvent& other) const;


    //# Content variables
  private:
    EventType             eventType = EventType::Default;
    static QSEventXMLTags xmlTags;

    struct {
        /// Ranges of time where this event will occur
        std::vector<spt::DateTimeRange> dateTimeRanges;
        std::vector<QSEventUptr>        nested; ///< Child events
        /// Time between starts of event ranges (for events that occur
        /// multiple times)
        QDateTime repeatInterval;
        int repeatTimes = 0; ///< How many times event will be repeated. -1
                             ///< for unlimited
        QDateTime               eventStart; ///< Start time of the event
        spt::TagSet             eventTags;  ///< Tags applied to this event
        std::vector<QSTodoUptr> todos; ///< Tasks associated with event
    } eventContent;

    struct {
        QColor color;
    } eventStyle;

    //# Member access funcitons
  public:
    void         setRepeatInterval(QDateTime interval);
    void         addEventTag(spt::Tag tag);
    void         setEventTags(spt::TagSet tags);
    spt::TagSet& getEventTagsRef();
    void         setRepeatTimes(int repeatTimes);
    void         appendTodo(QSTodoUptr todo);

    //#= DataEntity interface
  public:
    virtual QDomElement toXML(QDomDocument& document) const override;
    virtual void        fromXML(QXmlStreamReader* xmlStream) override;
    virtual QString     toDebugString(
            size_t indentationLevel = 0,
            bool   printEverything  = false,
            int    recurseLevel     = -1) override;
    virtual QString getExtension(bool prependDot) const override;
};
} // namespace qde
