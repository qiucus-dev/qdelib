//
// check it.

#include "qsevent.hpp"
#include "qsevent_xmlreader.hpp"
#include <hdebugmacro/all.hpp>


namespace qde {
QSEvent::QSEventXMLTags QSEvent::xmlTags;

bool QSEvent::operator==(const QSEvent& other) const {
    return CMP_DBG(
               DataItem::operator==(static_cast<const DataItem&>(other)))
           && CMP_DBG(
                  eventContent.eventTags == other.eventContent.eventTags)
           && CMP_DBG(
                  eventContent.repeatTimes
                  == other.eventContent.repeatTimes)
           && CMP_DBG(
                  eventContent.repeatInterval
                  == other.eventContent.repeatInterval)
           && CMP_DBG(
                  eventContent.nested.size()
                  == other.eventContent.nested.size())
           && CMP_DBG(std::equal(
                  eventContent.nested.begin(),
                  eventContent.nested.end(),
                  other.eventContent.nested.begin(),
                  other.eventContent.nested.end(),
                  [](const QSEventUptr& lhs, const QSEventUptr& rhs)
                      -> bool { return *lhs == *rhs; }));
}


void QSEvent::setRepeatInterval(QDateTime interval) {
    eventContent.repeatInterval = interval;
}


void QSEvent::addEventTag(spt::Tag tag) {
    eventContent.eventTags.addTag(tag);
}

spt::TagSet& QSEvent::getEventTagsRef() {
    return eventContent.eventTags;
}


void QSEvent::setEventTags(spt::TagSet tags) {
    eventContent.eventTags = tags;
}

void QSEvent::setRepeatTimes(int repeatTimes) {
    eventContent.repeatTimes = repeatTimes;
}


void QSEvent::appendTodo(QSTodoUptr todo) {
    eventContent.todos.push_back(std::move(todo));
}


QDomElement QSEvent::toXML(QDomDocument& document) const {
    QDomElement root = document.createElement(xmlTags.qsevent.rootName);

    root.appendChild(getBaseMetadataRef().toXML(document));

    QDomElement contentRoot = document.createElement(
        xmlTags.qsevent.content);

    contentRoot.appendChild(spt::list_to_xml(
        eventContent.dateTimeRanges,
        document,
        [&document](const spt::DateTimeRange& range) -> QDomElement {
            return range.toXML(document);
        },
        xmlTags.qsevent.dateTimeRanges));

    contentRoot.appendChild(spt::list_to_xml(
        eventContent.nested,
        document,
        [&document](const QSEventUptr& event) -> QDomElement {
            return event->toXML(document);
        },
        xmlTags.qsevent.nestedEvents));

    contentRoot.appendChild(spt::list_to_xml(
        eventContent.todos,
        document,
        [&document](const QSTodoUptr& event) -> QDomElement {
            return event->toXML(document);
        },
        xmlTags.qsevent.nestedTodos));

    contentRoot.appendChild(spt::textElement(
        document,
        xmlTags.qsevent.repeatInterval,
        eventContent.repeatInterval.toString(Qt::ISODate)));

    contentRoot.appendChild(spt::textElement(
        document,
        xmlTags.qsevent.repeatTimes,
        QString::number(eventContent.repeatTimes)));

    contentRoot.appendChild(spt::textElement(
        document,
        xmlTags.qsevent.eventStart,
        eventContent.eventStart.toString(Qt::ISODate)));

    contentRoot.appendChild(spt::textElement(
        document, xmlTags.qsevent.eventType, [&]() -> QString {
            switch (eventType) {
                case EventType::Default: return "Default";
                case EventType::AllDay: return "AllDay";
                case EventType::Background: return "Background";
            }
        }()));


    contentRoot.appendChild(
        eventContent.eventTags.toXML(document, xmlTags.qsevent.eventTags));

    root.appendChild(contentRoot);

    return root;
}


void QSEvent::fromXML(QXmlStreamReader* xmlStream) {
    xml::readQSEventXML(this, xmlStream);
}


QString QSEvent::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {
    QString result = DataItem::toDebugString(
        indentationLevel, printEverything, recurseLevel);

    static const int justify = xmlTags.base.debugStringSize;

    result += eventContent.eventTags.isEmpty() && !printEverything
                  ? ""
                  : dbg::justify("Event tags:", justify)
                        + QString::fromStdString(
                              eventContent.eventTags.toDebugString());

    result += eventContent.eventStart.isNull() && !printEverything
                  ? ""
                  : dbg::justify("Event start:", justify)
                        + eventContent.eventStart.toString(Qt::ISODate);

    result += eventContent.repeatInterval.isNull() && !printEverything
                  ? ""
                  : dbg::justify("Repeat interval:", justify)
                        + eventContent.repeatInterval.toString(
                              Qt::ISODate);

    if (recurseLevel != 0) {
        for (QSTodoUptr& todo : eventContent.todos) {
            result += todo->toDebugString(
                indentationLevel + 1, printEverything, recurseLevel);
        }
    }

    return result;
}


QString QSEvent::getExtension(bool prependDot) const {
    if (prependDot) {
        return "." + xmlTags.qsevent.extension;
    } else {
        return xmlTags.qsevent.extension;
    }
}

} // namespace qde
