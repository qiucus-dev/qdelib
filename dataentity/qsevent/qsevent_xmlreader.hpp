#pragma once

class QXmlStreamReader;

namespace qde {
class QSEvent;
namespace xml {
    void readQSEventXML(
        QSEvent*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags = nullptr);
} // namespace xml
} // namespace qde
