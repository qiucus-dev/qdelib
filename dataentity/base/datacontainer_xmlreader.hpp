#pragma once

#include "dataentity_xmlreader.hpp"
#include "datacontainer.hpp"

namespace qde {
namespace xml {
    class DataContainer_XMLReader : public DataEntity_XMLReader
    {
      public:
        DataContainer_XMLReader(QXmlStreamReader* _xmlStream);
        void setTarget(ContainerItem* value);

      private:
        ContainerItem* target;


        // DataEntity_XMLReader interface
      public:
        void readXML() override;
    };


} // namespace xml
} // namespace qde

