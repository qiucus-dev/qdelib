#pragma once

#include <hdebugmacro/logging.hpp>

#ifdef DENT_DEBUG
#    define DENT_LOG LOG
#    define DENT_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define DENT_FUNC_END DEBUG_FUNCTION_END
#    define DENT_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define DENT_LOG VOID_LOG
#    define DENT_FUNC_BEGIN
#    define DENT_FUNC_END
#    define DENT_FUNC_RET(message)
#endif
