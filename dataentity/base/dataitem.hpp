#pragma once


#include "dataentity.hpp"

namespace qde {
/*!
 * \brief Classes derived from this represent singular piece of data (one
 * note, one event etc.)
 */
class DataItem : public DataEntity
{
  public:
    DataItem();
};
} // namespace qde
