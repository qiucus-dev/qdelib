

#include "dataentity_support.hpp"

namespace qde {
SimpleData::SimpleData(QString _data) {
    content = _data;
}

// TODO WTF
bool SimpleData::operator==(const SimpleData& other) {
    return content.index() == other.content.index();
}

QDomElement SimpleData::toXML(QDomDocument& document) {
    QDomElement XMLRoot    = document.createElement("simpledata-root");
    QDomElement XMLType    = document.createElement("simpledata-type");
    QDomElement XMLContent = document.createElement("simpledata-content");
    XMLRoot.appendChild(XMLType);
    switch (getContentType()) {
        case Type::Int:
            XMLContent.appendChild(
                document.createTextNode(QString(std::get<int>(content))));
            XMLType.appendChild(document.createTextNode("int"));
            break;
        case Type::QString:
            XMLContent.appendChild(
                document.createTextNode(std::get<QString>(content)));
            XMLType.appendChild(document.createTextNode("string"));
            break;
        case Type::QStringList:
            XMLContent.appendChild(document.createTextNode(
                (std::get<QStringList>(content)).join(",")));
            XMLType.appendChild(document.createTextNode("stringlist"));
            break;
        case Type::Double:
            XMLContent.appendChild(document.createTextNode(
                QString::number(std::get<double>(content))));
            XMLType.appendChild(document.createTextNode("double"));
            break;
        case Type::LongLong:
            XMLContent.appendChild(document.createTextNode(
                QString::number(std::get<long long>(content))));
            XMLType.appendChild(document.createTextNode("longlong"));
            break;
        case Type::QUrl:
            XMLContent.appendChild(document.createTextNode(
                std::get<QUrl>(content).toString()));
            XMLType.appendChild(document.createTextNode("url"));
            break;
        case Type::QDateTime:
            XMLContent.appendChild(document.createTextNode(
                std::get<QDateTime>(content).toString()));
            XMLType.appendChild(document.createTextNode("datetime"));
            break;
        case Type::SingleSelect:
            XMLContent.appendChild(
                std::get<spt::SingleSelect>(content).toXML(document));
            XMLType.appendChild(document.createTextNode("singleselect"));
            break;
        case Type::MultiSelect:
            XMLContent.appendChild(
                std::get<spt::MultiSelect>(content).toXML(document));
            XMLType.appendChild(document.createTextNode("multiselect"));
            break;
    }
    XMLRoot.appendChild(XMLContent);
    return XMLRoot;
}

void SimpleData::fromXML(QXmlStreamReader* xmlStream) {
    QString elementName = xmlStream->name().toString();
    if (elementName != "simpledata-root") {
        return;
    }

    QString elementType;
    QString elementContent;

    while (xmlStream->readNextStartElement()) {
        elementName = xmlStream->name().toString();
        if (elementName == "simpledata-type") {
            elementType = xmlStream->readElementText();
        } else if (elementName == "simpledata-content") {
            elementContent = xmlStream->readElementText();
        } else {
            xmlStream->skipCurrentElement();
        }
    }
}

SimpleData::Type SimpleData::getContentType() const {
    switch (content.index()) {
        case 0: return Type::QString;
        case 1: return Type::QStringList;
        case 2: return Type::Int;
        case 3: return Type::Double;
        case 4: return Type::LongLong;
        case 5: return Type::QUrl;
        case 6: return Type::QDateTime;
        case 7: return Type::SingleSelect;
        case 8: return Type::MultiSelect;
    }
}

} // namespace qde
