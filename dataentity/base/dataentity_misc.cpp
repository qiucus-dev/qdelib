#include "dataentity.hpp"
#include <halgorithm/all.hpp>

namespace qde {
/*!
 * \brief Add pair of values to custom data map. If names collide old
 * values will be overwritten.
 */
void DataEntity::setCustomData(
    /// Key-value pair. First value will be used as key, second as value
    std::pair<QString, QString> data) {
    baseMetadata.customData[data.first] = data.second;
}


/*!
 * \brief Set source for DataEntity
 */
void DataEntity::setMetaSource(QString source) {
    baseMetadata.source = source;
}


/*!
 * \brief Get user-assigned name of the objects. Do not confuse with
 * extension or object type
 * \return Name of the object as QString without any formatting changes
 */
QString DataEntity::getMetaName() const {
    return baseMetadata.name;
}


QUuid DataEntity::getUUID() const {
    return baseMetadata.uuid;
}

DataDescriptor DataEntity::asDescriptor() const {
    DataDescriptor res;
    res.uuid      = baseMetadata.uuid;
    res.exactName = baseMetadata.exactName;
    return res;
}

// \brief Get description from base metadata
/// \return Content of DataEntity.base_metadata.object_description
QString DataEntity::getMetaDescription() const {
    return baseMetadata.description;
}


/// \brief Set description in base metadata field
void DataEntity::setMetaDescription(QString description) {
    baseMetadata.description = description;
}


/// \brief Add tag in base metadata field
void DataEntity::addMetaTags(const spt::TagSet& tags) {
    for (const spt::Tag& tag : tags) {
        if (!spt::contains(baseMetadata.tags.getTagsCRef(), tag)) {
            baseMetadata.tags.addTag(tag);
        }
    }
}


/// \brief Add tag in base metadata field
void DataEntity::addMetaTag(spt::Tag tag) {
    if (!spt::contains(baseMetadata.tags.getTagsCRef(), tag)) {
        baseMetadata.tags.addTag(tag);
    }
}


/// \brief Set base metadata tags
void DataEntity::setMetaTags(const spt::TagSet& tags) {
    baseMetadata.tags = std::move(tags);
}


/// \brief Get base metatada tags
spt::TagSet DataEntity::getMetaTags() {
    return baseMetadata.tags;
}


/// \brief Set creation DateTime to QDateTime::currentDateTime
void DataEntity::setCreationNow() {
    baseMetadata.creationDateTime = QDateTime::currentDateTime();
}


QString DataEntity::getMetaNote() const {
    return baseMetadata.metaNote;
}


void DataEntity::setMetaNote(QString notes) {
    baseMetadata.metaNote = notes;
}

void DataEntity::setMetaName(QString name) {
    baseMetadata.name = name;
}

void DataEntity::setExactName(const QString& name) {
    baseMetadata.exactName = name;
}


BaseMetadata DataEntity::getBaseMetadata() const {
    return baseMetadata;
}


void DataEntity::setBaseMetadata(const BaseMetadata& value) {
    baseMetadata = value;
}


BaseMetadata& DataEntity::getBaseMetadataRef() {
    return baseMetadata;
}

const BaseMetadata& DataEntity::getBaseMetadataRef() const {
    return baseMetadata;
}
} // namespace qde
