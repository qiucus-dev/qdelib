#pragma once

#include <hdebugmacro/all.hpp>

/// \todo Move to DebugMacro main module

#define XMLREADER_PARSER_DEBUG

#ifdef XMLREADER_PARSER_DEBUG
#    define XML_LOG LOG
#    define XML_WARN WARN
#    define XML_INFO INFO
#    define XML_ERROR ERROR
#    define XML_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define XML_FUNC_END DEBUG_FUNCTION_END
#    define XMLREADER_SKIP_ELEMENT(xml_element_name)                      \
        XML_INFO << "Skipping element" << xml_element_name;

#    define XMLREADER_READ_ELEMENT(xml_element_name)                      \
        XML_INFO << "Reading element" << xml_element_name;

#    define EXIT_IF_NOT_START_OR_WRONG_NAME(targetElementName)            \
        if (xmlStream->name() != targetElementName) {                     \
            XML_ERROR << "Wrong element";                                 \
            DEBUG_INDENT                                                  \
            XML_INFO << "Expected:" << targetElementName;                 \
            XML_INFO << "Got     :" << xmlStream->name();                 \
            DEBUG_INTERNAL_POSITION_IN_CODE                               \
            DEBUG_DEINDENT                                                \
            return;                                                       \
        } else if (!xmlStream->isStartElement()) {                        \
            XML_ERROR << "Not start element" << targetElementName;        \
            DEBUG_INDENT                                                  \
            DEBUG_INTERNAL_POSITION_IN_CODE                               \
            DEBUG_DEINDENT                                                \
            return;                                                       \
        }


#    define XMLREADER_START_READING                                       \
        XML_FUNC_BEGIN                                                    \
        DEBUG_ASSERT_TRUE(target != nullptr)                              \
        if (xmlStream->name().isEmpty()) {                                \
            xmlStream->readNextStartElement();                            \
        }                                                                 \
        EXIT_IF_NOT_START_OR_WRONG_NAME(target->getXMLRoot())

#else
#    define XML_FUNC_BEGIN
#    define XML_FUNC_END
#    define EXIT_IF_NOT_START_OR_WRONG_NAME(targetElementName)            \
        if (xmlStream->name() != targetElementName) {                     \
            return;                                                       \
        } else if (!xmlStream->isStartElement()) {                        \
            return;                                                       \
        }

#    define XMLREADER_START_READING                                       \
        if (xmlStream->name().isEmpty()) {                                \
            xmlStream->readNextStartElement();                            \
        }                                                                 \
        EXIT_IF_NOT_START_OR_WRONG_NAME(target->getXMLRoot())


#    define XMLREADER_SKIP_ELEMENT(xml_element_name)
#    define XMLREADER_READ_ELEMENT(xml_element_name)
#    define XML_LOG VOID_LOG
#endif

#define EXIT_IF_NOT_START                                                 \
    if (!xmlStream->isStartElement()) {                                   \
        return;                                                           \
    }


#define EXIT_IF_WRONG_NAME(name)                                          \
    if (xmlStream->name() != name) {                                      \
        return;                                                           \
    }
