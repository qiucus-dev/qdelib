

#include "dataentity_xmlreader.hpp"
#include "dataentity.hpp"

namespace qde {
namespace xml {
    DataEntity_XMLReader::DataEntity_XMLReader(
        QXmlStreamReader* _xmlStream) {
        xmlStream = _xmlStream;
    }

    DataEntity_XMLReader::~DataEntity_XMLReader() {
    }

    void DataEntity_XMLReader::readXML() {
    }

    DataEntity* DataEntity_XMLReader::getTarget() const {
        return target;
    }

    void DataEntity_XMLReader::setTarget(DataEntity* value) {
        target = value;
    }

    //    SimpleData
    //        DataEntity_XMLReader::readSimpleData(QXmlStreamReader*
    //        xmlStream) { QString elementName =
    //        xmlStream->name().toString(); if (elementName !=
    //        "qstable-item-simple") {
    //            return SimpleData();
    //        }

    //        xmlStream->readNextStartElement();
    //        QString itemType;
    //        if (elementName == "item-type") {
    //            itemType = xmlStream->readElementText();
    //        }

    //        bool isBase64;
    //        xmlStream->readNextStartElement();
    //        elementName = xmlStream->name().toString();
    //        if (elementName == "is-base64") {
    //            isBase64 = (xmlStream->readElementText() == "true");
    //        }

    //        SimpleData simpleData;

    //        xmlStream->readNextStartElement();
    //        elementName = xmlStream->name().toString();
    //        if (elementName == "item-content") {
    //            QString itemContent = xmlStream->readElementText();
    //            if (isBase64) {
    //                QByteArray base64Content =
    //                    QByteArray::fromStdString(itemContent.toStdString());
    //                simpleData.setContent(itemType, itemContent);
    //            } else {
    //                simpleData.setContent(itemType, itemContent);
    //            }
    //        }

    //        return simpleData;
    //    }
} // namespace xml
} // namespace qde
