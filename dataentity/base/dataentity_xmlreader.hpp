#pragma once

#include "dataentity_support.hpp"
#include <QDomNode>
#include <QString>
#include <QXmlStreamReader>


namespace qde {
namespace xml {
    class DataEntity;
    class DataEntity_XMLReader
    {
      public:
        explicit DataEntity_XMLReader(QXmlStreamReader* _xmlStream);
        virtual ~DataEntity_XMLReader();
        virtual void readXML();
        DataEntity*  getTarget() const;
        void         setTarget(DataEntity* value);
        //        virtual SimpleData readSimpleData(QXmlStreamReader*
        //        xmlStream);

      protected:
        QXmlStreamReader* xmlStream;

      private:
        DataEntity* target;
    };
} // namespace xml
} // namespace qde
