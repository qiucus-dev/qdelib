#include "datacontainer_xmlreader.hpp"
#include <qdelib/qslink.hpp>
#include <qdelib/qsnote.hpp>
#include <qdelib/qstable.hpp>


namespace qde {

void ContainerItem::fromXML(QXmlStreamReader* xmlStream) {
    xml::DataContainer_XMLReader xmlReader(xmlStream);
    xmlReader.setTarget(this);
    xmlReader.readXML();
}

QString ContainerItem::getXMLRoot() const {
    return xmlTags.rootName;
}


namespace xml {
    DataContainer_XMLReader::DataContainer_XMLReader(
        QXmlStreamReader* _xmlStream)
        : DataEntity_XMLReader(_xmlStream) {
    }

    void DataContainer_XMLReader::setTarget(ContainerItem* value) {
        target = value;
    }

    void DataContainer_XMLReader::readXML() {
        XMLREADER_START_READING

        while (xmlStream->readNextStartElement()) {
            QString elementName = xmlStream->name().toString();
            if (elementName == "qsnote-root") {
                std::unique_ptr<qde::QSNote>
                    note = std::make_unique<qde::QSNote>();
                note->fromXML(xmlStream);
                target->setContent(std::move(note));
            } else if (elementName == "qslink-root") {
                std::unique_ptr<qde::QSLink>
                    link = std::make_unique<qde::QSLink>();
                link->fromXML(xmlStream);
                target->setContent(std::move(link));
            } else if (elementName == "qstable-root") {
                std::unique_ptr<qde::QSTable>
                    table = std::make_unique<qde::QSTable>();
                table->fromXML(xmlStream);
                target->setContent(std::move(table));
            } else if (elementName == "simpledata-root") {
                SimpleData data;
                data.fromXML(xmlStream);
                target->setContent(data);
            } else {
                xmlStream->skipCurrentElement();
            }
        }

        XML_FUNC_END
    }

} // namespace xml
} // namespace qde
