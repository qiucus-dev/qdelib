#include "base_metadata.hpp"
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>
#include <hsupport/xml.hpp>


namespace qde {

QDomElement Attachment::toXML(QDomDocument& document) const {
    BaseMetadata::XMLTags& tags = BaseMetadata::xmlTags;
    QDomElement            res  = document.createElement(
        tags.attachment.attachmentName);

    res.setAttribute(tags.attachment.path, path);

    return res;
}

void Attachment::readXML(QXmlStreamReader* xmlStream) {
    BaseMetadata::XMLTags& tags       = BaseMetadata::xmlTags;
    QXmlStreamAttributes   attributes = xmlStream->attributes();

    path = attributes.value(tags.attachment.path).toString();
    xmlStream->skipCurrentElement();
}


bool DataDescriptor::operator==(const DataDescriptor& other) const {
    return uuid == other.uuid && exactName == other.exactName;
}

QDomElement DataDescriptor::toXML(QDomDocument& document) const {
    BaseMetadata::XMLTags& tags = BaseMetadata::xmlTags;

    QDomElement res = document.createElement(
        tags.descriptor.descriptorName);

    res.setAttribute(
        tags.descriptor.uuid, uuid.toString(QUuid::WithoutBraces));

    if (exactName.has_value()) {
        res.setAttribute(tags.descriptor.exactName, exactName.value());
    }

    return res;
}

void DataDescriptor::readXML(QXmlStreamReader* xmlStream) {
    QXmlStreamAttributes attributes = xmlStream->attributes();

    if (attributes.hasAttribute(
            BaseMetadata::xmlTags.descriptor.exactName)) {
        exactName = attributes
                        .value(
                            "", BaseMetadata::xmlTags.descriptor.exactName)
                        .toString();
    }

    uuid = QUuid::fromString(
        attributes.value("", BaseMetadata::xmlTags.descriptor.uuid)
            .toString());

    xmlStream->skipCurrentElement();
}


qde::BaseMetadata::XMLTags qde::BaseMetadata::xmlTags;


QDomElement BaseMetadata::toXML(QDomDocument& document) const {
    QDomElement root = document.createElement(xmlTags.base.section);

    root.appendChild(spt::textElement(
        document, xmlTags.base.uuid, uuid.toString(QUuid::WithoutBraces)));

    if (!description.isEmpty()) {
        root.appendChild(spt::textElement(
            document, xmlTags.base.description, description, true));
    }

    if (!metaNote.isEmpty()) {
        root.appendChild(spt::textElement(
            document, xmlTags.base.metaNote, metaNote, true));
    }

    root.appendChild(spt::textElement(
        document,
        xmlTags.base.creationDateTime,
        creationDateTime.toTimeSpec(Qt::OffsetFromUTC)
            .toString(Qt::ISODate)));

    if (!tags.isEmpty()) {
        root.appendChild(tags.toXML(document, xmlTags.base.tags));
    }

    if (editDateTime.isValid()) {
        root.appendChild(spt::textElement(
            document,
            xmlTags.base.editDateTime,
            editDateTime.toTimeSpec(Qt::OffsetFromUTC)
                .toString(Qt::ISODate)));
    }

    if (!name.isEmpty()) {
        root.appendChild(
            spt::textElement(document, xmlTags.base.name, name, true));
    }

    if (!source.isEmpty()) {
        root.appendChild(
            spt::textElement(document, xmlTags.base.source, source, true));
    }

    if (!exactName.isEmpty()) {
        root.appendChild(spt::textElement(
            document, xmlTags.base.exactName, exactName, true));
    }

    if (!version.isDefault()) {
        root.appendChild(version.toXML(document, xmlTags.base.version));
    }

    if (!version.isDefault()) {
        root.appendChild(
            xmlVersion.toXML(document, xmlTags.base.xmlVersion));
    }

    if (customData.size() != 0) {
        root.appendChild(spt::map_to_xml(
            customData,
            document,
            [&document](
                const std::pair<QString, QString>& pair) -> QDomElement {
                QDomElement res = document.createElement("pair");
                res.appendChild(
                    spt::textElement(document, "key", pair.first, true));
                res.appendChild(spt::textElement(
                    document, "value", pair.second, true));
                return res;
            },
            xmlTags.base.customData));
    }

    return root;
}


// TODO Think about replacing all duplicated lines (there is a lot of code
// that repeats itself with only minor changes) with macro. It is possible
// to define macro just before the function to make it easier to understand
void BaseMetadata::readXML(QXmlStreamReader* xmlStream) {
    while (xmlStream->readNextStartElement()) {
        if (xmlStream->name() == xmlTags.base.uuid) {
            uuid = QUuid::fromString(xmlStream->readElementText());

        } else if (xmlStream->name() == xmlTags.base.description) {
            description = xmlStream->readElementText();

        } else if (xmlStream->name() == xmlTags.base.creationDate) {
            creationDateTime.setDate(QDate::fromString(
                xmlStream->readElementText(), Qt::DateFormat::ISODate));

        } else if (xmlStream->name() == xmlTags.base.creationTime) {
            creationDateTime.setTime(
                QTime::fromString(xmlStream->readElementText()));

        } else if (xmlStream->name() == xmlTags.base.editDate) {
            editDateTime.setDate(QDate::fromString(
                xmlStream->readElementText(), Qt::DateFormat::ISODate));

        } else if (xmlStream->name() == xmlTags.base.editTime) {
            editDateTime.setTime(
                QTime::fromString(xmlStream->readElementText()));

        } else if (xmlStream->name() == xmlTags.base.tags) {
            tags = spt::TagSet::fromXML(xmlStream, xmlTags.base.tags);

        } else if (xmlStream->name() == xmlTags.base.metaNote) {
            metaNote = xmlStream->readElementText();

        } else if (xmlStream->name() == xmlTags.base.name) {
            name = xmlStream->readElementText();

        } else if (xmlStream->name() == xmlTags.base.source) {
            source = xmlStream->readElementText();

        } else if (xmlStream->name() == xmlTags.base.exactName) {
            exactName = xmlStream->readElementText();

        } else if (xmlStream->name() == xmlTags.base.version) {
            version.readXML(xmlStream);

        } else if (xmlStream->name() == xmlTags.base.xmlVersion) {
            xmlVersion.readXML(xmlStream);

        } else if (xmlStream->name() == xmlTags.base.customData) {
            spt::map_from_xml(
                customData,
                xmlStream,
                [](std::map<QString, QString>&  map,
                   std::pair<QString, QString>& pair) {
                    map[pair.first] = pair.second;
                },
                xmlTags.base.customData);

        } else {
            xmlStream->skipCurrentElement();
        }
    }
}

bool BaseMetadata::operator==(const BaseMetadata& other) const {
    LOG << "Comparing";
    LOG << "name" << name;
    LOG << "other.name" << other.name;

    return CMP_DBG(uuid == other.uuid)                    //
           && CMP_DBG(editDateTime == other.editDateTime) //
           && CMP_DBG(description == other.description)   //
           && CMP_DBG(exactName == other.exactName)       //
           && CMP_DBG(name == other.name)                 //
           && CMP_DBG(source == other.source)             //
           && CMP_DBG(version == other.version)           //
           && CMP_DBG(metaNote == other.metaNote)         //

           // FIXME sometimes this returns false value even if dates are
           // equal (when printing)
           //   && CMP_DBG(creationDateTime == other.creationDateTime)
           && CMP_DBG(std::equal(
                  customData.begin(),
                  customData.end(),
                  other.customData.begin(),
                  other.customData.end(),
                  [](std::pair<QString, QString> lhs,
                     std::pair<QString, QString> rhs) {
                      return lhs.first == rhs.first
                             && lhs.second == rhs.second;
                  })) //

        ; //
}


QString BaseMetadata::toDebugString(
    size_t               indentationLevel,
    bool                 printEverything,
    [[maybe_unused]] int recurseLevel) const {

    QString padding(indentationLevel * xmlTags.base.nestedIndentSize, ' ');

    QString          res;
    static const int justify = xmlTags.base.debugStringSize;

    res += padding + "==== BaseMetadata::toDebugString:\n";
    res += tags.isEmpty() || printEverything
               ? ""
               : padding + dbg::justify("Tags:", justify) + "\n"
                     + QString::fromStdString(tags.toDebugString(
                           indentationLevel
                           * xmlTags.base.nestedIndentSize))
                     + "\n";

    res += description.isEmpty() && !printEverything
               ? ""
               : padding + dbg::justify("Description:", justify)
                     + description + "\n";

    res += name.isNull() && !printEverything
               ? ""
               : padding + dbg::justify("Name:", justify) + name + "\n";

    res += creationDateTime.isNull() && !printEverything
               ? ""
               : padding + dbg::justify("Creation datetime:", justify)
                     + creationDateTime.toTimeSpec(Qt::OffsetFromUTC)
                           .toString(Qt::ISODate)
                     + "\n";

    res += editDateTime.isNull() && !printEverything
               ? ""
               : padding + dbg::justify("Edit datetime:", justify)
                     + editDateTime.toTimeSpec(Qt::OffsetFromUTC)
                           .toString(Qt::ISODate)
                     + "\n";

    res += metaNote.isEmpty() && !printEverything
               ? ""
               : padding + dbg::justify("Meta note:", justify) + metaNote
                     + "\n";

    res += source.isEmpty() && !printEverything
               ? ""
               : padding + dbg::justify("Source:", justify) + source
                     + "\n";

    res += (customData.size() == 0) && !printEverything
               ? ""
               : padding + [this, &padding]() -> QString {
        QString res = "Custom data:\n";
        for (const std::pair<const QString, QString>& record :
             customData) {
            res += padding + dbg::justify("  Name:", justify)
                   + record.first + "\n";

            // TODO content should start on the same line with Content:
            QString prefix = padding + dbg::justify("  Content:", justify)
                             + "\n";
            res += prefix;
            {
                QString padding(prefix.size() - 1, ' ');
                for (QString& str : record.second.split("\n")) {
                    res += padding + str + "\n";
                }
            }
        }
        return res;
    }();

    res += uuid.isNull() && !printEverything
               ? ""
               : padding + dbg::justify("UUID:", justify)
                     + uuid.toString(QUuid::WithoutBraces) + "\n";

    res += (version.isDefault() && !printEverything)
               ? ""
               : padding + dbg::justify("Version:", justify)
                     + QString::fromStdString(version.toDebugString())
                     + "\n";

    return res;
}
} // namespace qde
