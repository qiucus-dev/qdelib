#pragma once

#include "dataentity.hpp"
#include "dataentity_support.hpp"
#include <QXmlStreamReader>
#include <variant>

namespace qde {
typedef std::variant<SimpleData, std::unique_ptr<DataEntity>>
    ContainerVariant;
/*!
 * \brief The ContainerItem class is a wrapper class for storing various
 * dataentity derived objects and simple data in single objects.
 */
class ContainerItem
{
  public:
    ContainerItem() = default;

    struct ContainerItemXMLTags {
        QString rootName;
    };

    template <class T>
    ContainerItem(std::unique_ptr<T>&& _data) {
        static_assert(
            std::is_convertible<T*, DataEntity*>::value,
            "Only classes derived from DataEntity can be used "
            "as parameter for ContainerItem::setContent");
        data = std::move(_data);
    }

    ContainerItem(const ContainerItem& other);
    ContainerItem& operator=(const ContainerItem& other);
    ContainerItem& operator=(ContainerItem&& other);
    ContainerItem& operator=(const SimpleData& other);
    bool           operator==(const ContainerItem& other);


    bool        isSimple() const;
    QDomElement toXML(QDomDocument& document);
    QVariant    toVariant();
    QString     getName();
    void        fromXML(QXmlStreamReader* xmlStream);
    QString     getXMLRoot() const;


    template <class T>
    void setContent(std::unique_ptr<T> _data) {
        static_assert(
            std::is_convertible<T*, DataEntity*>::value,
            "Only classes derived from DataEntity can be used "
            "as parameter for ContainerItem::setContent");
        data = std::move(_data);
    }

    void setContent(SimpleData _data);

  private:
    static ContainerItemXMLTags xmlTags;
    ContainerVariant            data;
};

using ContainerItemUptr = std::unique_ptr<ContainerItem>;

} // namespace qde
