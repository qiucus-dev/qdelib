## TODO

[ ] All classes should have associated UPtr alias defined in the same header file
[ ] Access-by-index operators should accept size_t instead of an int
[ ] Check variable naming in xml readers/writers
[ ] XMLReaders should forward declare target class
[ ] Xml readers should use pointer to xml tags instead of direct access to
    static member

## TODO IMPORTANT

[ ] Add version saving to the BaseMetadata xml
[ ] Add nested tags
