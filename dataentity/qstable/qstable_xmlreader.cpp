#include "qstable_xmlreader.hpp"
#include "qstable.hpp"
#include <QXmlStreamReader>
#include <qdelib/qslink.hpp>
#include <qdelib/qsnote.hpp>


namespace qde {
namespace xml {
    struct ParseParams {
        QSTable*                 target;
        QXmlStreamReader*        xmlStream;
        QSTable::QSTableXMLTags* tags;
        int                      currentPageIDX = 0;
        int                      currentRowIDX  = 0;
        int                      currentItemIDX = 0;
    };


    static void readPageElement(ParseParams& params, int row, int column) {
        while (params.xmlStream->readNextStartElement()) {
            QString elementName = params.xmlStream->name().toString();
            if (params.xmlStream->name() == ContainerItem().getXMLRoot()) {
                std::optional<TablePage*> page = params.target->getPage(
                    params.currentPageIDX);
                if (page.has_value()) {
                    ContainerItemUptr item = std::make_unique<
                        ContainerItem>();
                    item->fromXML(params.xmlStream);
                    page.value()->setAt(std::move(item), row, column);
                }
            } else {
                params.xmlStream->skipCurrentElement();
            }
        }
    }


    static void readPageRow(ParseParams& params, int currentRowIDX) {
        while (params.xmlStream->readNextStartElement()) {
            if (params.xmlStream->name()
                == params.tags->qstable.tableItem) {
                readPageElement(
                    params, currentRowIDX, params.currentItemIDX);
                params.currentItemIDX++;
            } else {
                params.xmlStream->skipCurrentElement();
            }
        }
        params.currentItemIDX = 0;
    }


    static void readOneTablePage(ParseParams& params) {
        while (params.xmlStream->readNextStartElement()) {
            if (params.xmlStream->name() == params.tags->qstable.pageRow) {
                readPageRow(params, params.currentRowIDX);
                params.currentRowIDX++;
            } else {
                params.xmlStream->skipCurrentElement();
            }
        }
        params.currentRowIDX = 0;
    }


    static void readTablePages(ParseParams& params) {
        while (params.xmlStream->readNextStartElement()) {
            if (params.xmlStream->name() == "qstable-page") {
                QXmlStreamAttributes attributes = params.xmlStream
                                                      ->attributes();
                int pageIDX = attributes.value("idx").toInt();
                int rowNum  = attributes.value("rows").toInt();
                int colNum  = attributes.value("columns").toInt();
                params.currentPageIDX          = pageIDX;
                std::optional<TablePage*> page = params.target->getPage(
                    pageIDX);
                if (page.has_value()) {
                    page.value()->resize(rowNum, colNum);
                }
                readOneTablePage(params);
            } else {
                params.xmlStream->skipCurrentElement();
            }
        }
    }

    static void readQSTableContent(ParseParams& params) {
        while (params.xmlStream->readNextStartElement()) {
            if (params.xmlStream->name() == "qstable-pages") {
                QXmlStreamAttributes attributes = params.xmlStream
                                                      ->attributes();
                int pageCount = attributes.value("page-count").toInt();
                params.target->resize(pageCount);
                readTablePages(params);
            } else {
                params.xmlStream->skipCurrentElement();
            }
        }
    }


    void readQSTableXML(
        QSTable*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags) {
        if (xmlStream->name().isEmpty()) {
            xmlStream->readNextStartElement();
        }

        QSTable::QSTableXMLTags* tags;
        if (_tags == nullptr) {
            tags = &target->xmlTags;
        } else {
            tags = static_cast<QSTable::QSTableXMLTags*>(_tags);
        }

        ParseParams params;
        params.target    = target;
        params.tags      = tags;
        params.xmlStream = xmlStream;

        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == "metadata-common") {
                target->getBaseMetadataRef().readXML(xmlStream);
            } else if (xmlStream->name() == "qstable-content") {
                readQSTableContent(params);
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }

} // namespace xml
} // namespace qde
