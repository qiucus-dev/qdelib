#include "qstable.hpp"
#include "qstable_xmlreader.hpp"
#include <halgorithm/all.hpp>


namespace qde {
QSTable::QSTableXMLTags QSTable::xmlTags;


std::optional<ContainerItem*> QSTable::at(
    size_t row,
    size_t column,
    size_t page) {
    if (pages.size() <= page
        || !pages[page]->at(row, column).has_value()) {
        return std::optional<ContainerItem*>();
    } else {
        return pages[page]->at(row, column).value();
    }
}

TablePage& QSTable::operator[](size_t index) {
    if (pages.size() <= index) {
        throw std::invalid_argument("Index is out of range");
    } else {
        return (*pages.at(index));
    }
}


void QSTable::setAt(
    ContainerItem item,
    size_t        row,
    size_t        col,
    size_t        page,
    bool          check) {
    if (check
        && (pages.size() <= page
            || !pages[page]->at(row, col).has_value())) {
        throw std::invalid_argument("Index is out of range");
    }

    *(this->at(row, col, page).value()) = std::move(item);
}


/*!
 * \brief QSTable::resize: set page counts to pageCount number. Missing
 * pages will be created. Excess pages will be deleted
 */
void QSTable::resize(uint pageCount) {
    spt::resize_assign(
        pages, pageCount, []() { return std::make_unique<TablePage>(); });
}


void QSTable::resize(size_t pageCount, size_t rowCount, size_t colCount) {
    spt::resize_assign(
        pages, pageCount, []() { return std::make_unique<TablePage>(); });
    for (TablePageUptr& page : pages) {
        page->resize(rowCount, colCount);
    }
}


/// Return pointer to page located at index or default-constructed
/// std::optional
std::optional<TablePage*> QSTable::getPage(size_t index) const {
    if (!(index <= pages.size())) {
        return std::optional<TablePage*>();
    } else {
        return std::optional<TablePage*>(pages.at(index).get());
    }
}


/*!
 * \brief Return vector with pointer to each page in the table
 */
std::vector<TablePage*> QSTable::getPages() const {
    std::vector<TablePage*> result;
    result.reserve(pages.size());
    for (const std::unique_ptr<TablePage>& page : pages) {
        result.push_back(page.get());
    }

    return result;
}


uint QSTable::size() const {
    return pages.size();
}


void QSTable::setAt(
    SimpleData _data,
    size_t     row,
    size_t     column,
    size_t     page) {
    *(this->at(row, column, page).value()) = _data;
}


QString QSTable::getExtension(bool prependComma) const {
    if (prependComma) {
        return "." + xmlTags.qstable.extension;
    } else {
        return xmlTags.qstable.extension;
    }
}


QDomElement QSTable::toXML(QDomDocument& document) const {
    QDomElement root = document.createElement(xmlTags.qstable.rootName);
    return root;
}


void QSTable::clear() {
    pages.clear();
}


void QSTable::fromXML(QXmlStreamReader* xmlStream) {
    xml::readQSTableXML(this, xmlStream);
}


QString QSTable::toDebugString(
    size_t indentationLevel,
    bool   printEverything,
    int    recurseLevel) {
    return DataContainer::toDebugString(
        indentationLevel, printEverything, recurseLevel);
}


std::optional<ContainerItem*> TablePage::at(size_t row, size_t col) {
    if (array.size() <= row || array[row]->size() <= col) {
        return std::optional<ContainerItem*>();
    } else {
        return array.at(row)->at(col);
    }
}

std::optional<ContainerItem*> TablePage::at(size_t row, size_t col) const {
    if (array.size() <= row || array[row]->size() <= col) {
        return std::optional<ContainerItem*>();
    } else {
        return array.at(row)->at(col);
    }
}

TableColumn& TablePage::operator[](size_t index) {
    if (array.size() <= index) {
        throw std::invalid_argument("Index is out of range");
    } else {
        return *array.at(index);
    }
}


int TablePage::getRowNum() const {
    return array.size();
}


int TablePage::getColumnNum(size_t rowIdx) const {
    return array[rowIdx]->size();
}


void TablePage::resize(size_t rowCount, size_t columnCount) {
    spt::resize_assign(
        array, rowCount, []() { return std::make_unique<TableColumn>(); });
    for (auto& col : array) {
        col->resize(columnCount);
    }
}

/// Return number of columns in the page
size_t TablePage::size() const {
    return array.size();
}


void TablePage::setAt(
    ContainerItemUptr item,
    size_t            row,
    size_t            column,
    bool              check) {
    if (check && (array.size() <= row || array[row]->size() <= column)) {
        throw std::invalid_argument("Index is out of range");
    }

    array.at(row)->setAt(std::move(item), column);
}

/*!
 * \brief Clear contend of the page. Does not  change actual size of the
 * page i.e. amount of rows and columns in each row remains intact
 */
void TablePage::clear() {
    array.clear();
}


std::optional<ContainerItem*> TableColumn::at(size_t column) {
    if (cells.size() <= column) {
        return std::optional<ContainerItem*>();
    } else {
        return std::optional<ContainerItem*>(cells.at(column).get());
    }
}

ContainerItem& TableColumn::operator[](size_t index) {
    if (cells.size() <= index) {
        throw std::invalid_argument("Index is out of range");
    } else {
        return *cells.at(index);
    }
}

size_t TableColumn::size() const {
    return cells.size();
}

void TableColumn::setAt(ContainerItemUptr item, size_t row, bool check) {
    if (check && cells.size() <= row) {
        throw std::invalid_argument("Index is out of range");
    }

    cells[row] = std::move(item);
}

void TableColumn::resize(uint targetSize) {
    spt::resize_assign(cells, targetSize, []() {
        return std::make_unique<ContainerItem>();
    });
}
} // namespace qde
