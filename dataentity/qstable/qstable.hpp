#pragma once

/*!
 * \\file qstable.hpp
 */

//===    Qt    ===//
#include <QDomElement>
#include <QXmlStreamReader>


//===    STL   ===//
#include <memory>
#include <optional>
#include <typeinfo>


//=== Internal ===//
#include <hsupport/misc.hpp>


//=== Sibling  ===//
#include "../base/datacontainer.hpp"
#include "qstable_xmlreader.hpp"


//  ////////////////////////////////////


namespace qde {

/*!
 * \brief The TableColumn class represents single row in the table
 */
class TableColumn
{
  public:
    std::optional<ContainerItem*> at(size_t column);

    ContainerItem& operator[](size_t index);

    size_t size() const;
    void   setAt(ContainerItemUptr item, size_t row, bool check = true);
    void   resize(uint targetSize);

  private:
    std::vector<ContainerItemUptr> cells;
};

/*!
 * \brief The TablePage class represents single page in the table.
 *
 * \note Page might not be rectangular i.e. it is not guaranteed that
 * amount of columns in each row is identical.
 *
 * Page is made of std::vector of std::unique_ptr of TableColumns
 */
class TablePage
{
  public:
    std::optional<ContainerItem*> at(size_t row, size_t column);
    std::optional<ContainerItem*> at(size_t row, size_t column) const;

    TableColumn& operator[](size_t index);

    int    getRowNum() const;
    int    getColumnNum(size_t rowIdx = 0) const;
    void   resize(size_t rowCount, size_t columnCount);
    size_t size() const;
    void   setAt(
          ContainerItemUptr item,
          size_t            row,
          size_t            column = 0,
          bool              check  = true);
    void clear();

  private:
    /// 2d array of cells. First index is rows, second index is columns.
    /// No checks for squaredness should be performed
    std::vector<std::unique_ptr<TableColumn>> array;
};

using TablePageUptr = std::unique_ptr<TablePage>;

namespace xml {
    class QSTable_XMLReader;
};

/*!
 * \brief The QSTable class represets list of 2d cheets, similar to Excel
 *
 * \todo Write access iterator that will allow to use constuctions like
 *
 * \code
 * for(TablePageUPtr& page : table) {
 *     for(TableRowUptr& row : page) {
 *         for(ContainerItemUptr& cell : row) {
 *
 *         }
 *      }
 * }
 * \endcode
 */
class QSTable : public DataContainer
{
    friend void xml::readQSTableXML(
        QSTable*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags);

  public:
    //= XML
    struct QSTableXMLTags : BaseMetadata::XMLTags {
        struct {
            QString section   = "qstable-content";
            QString rootName  = "qstable-root";
            QString extension = "qstable";
            QString pages     = "qstable-pages";
            QString page      = "qstable-page";
            QString pageCount = "page-count";
            QString pageRow   = "qstable-page-row";
            QString tableItem = "qstable-item";
        } qstable;
    };

    //= Special member functions
    QSTable()                   = default;
    virtual ~QSTable() override = default;

    //= Member access
    std::optional<TablePage*>     getPage(size_t index) const;
    std::vector<TablePage*>       getPages() const;
    std::optional<ContainerItem*> at(
        size_t row,
        size_t col  = 0,
        size_t page = 0);
    TablePage& operator[](size_t index);
    void       setAt(
              SimpleData _data,
              size_t     row,
              size_t     column = 0,
              size_t     page   = 0);
    void setAt(
        ContainerItem item,
        size_t        row,
        size_t        col   = 0,
        size_t        page  = 0,
        bool          check = true);

    //= Resizing
    void resize(uint size);
    void resize(size_t pageCount, size_t rowCount, size_t colCount);
    void resizePage(size_t columnCount, size_t rowCount, size_t pageNum);
    uint size() const;
    void clear();

  private:
    static QSTableXMLTags                   xmlTags;
    std::vector<std::unique_ptr<TablePage>> pages;

    //#= DataEntity interface
  public:
    QString     getExtension(bool prependComma = false) const override;
    QDomElement toXML(QDomDocument& document) const override;
    void        fromXML(QXmlStreamReader* xmlStream) override;
    QString     toDebugString(
            size_t indentationLevel = 0,
            bool   printEverything  = false,
            int    recurseLevel     = -1) override;
};

} // namespace qde


Q_DECLARE_METATYPE(qde::QSTable*)
