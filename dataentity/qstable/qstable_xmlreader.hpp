#pragma once

class QXmlStreamReader;

namespace qde {
class QSTable;
namespace xml {
    void readQSTableXML(
        QSTable*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags = nullptr);
} // namespace xml
} // namespace qde
