cmake_minimum_required(VERSION 3.6)

find_package(PythonInterp 3)
find_package(PythonLibs 3 REQUIRED)

find_package(Qt5Core CONFIG REQUIRED)
find_package(Qt5Xml CONFIG REQUIRED)
find_package(Qt5Gui CONFIG REQUIRED)

add_library(
    QDELib SHARED
    )

target_sources(
    QDELib PUBLIC
    QDELib.cpp
    QDELib_wrap.cxx
    )

set_target_properties(
    QDELib
    PROPERTIES PREFIX "_"
               SUFFIX ".so"
    )

set_property(
    TARGET
    QDELib
    PROPERTY
    CXX_STANDARD
    17)

target_include_directories(
    QDELib PUBLIC
    ${PYTHON_INCLUDE_DIRS}
    # Qt
    ${Qt5Core_INCLUDE_DIRS}
    ${Qt5Xml_INCLUDE_DIRS}
    ${Qt5Gui_INCLUDE_DIRS}
    # QDELib
    ${CMAKE_CURRENT_SOURCE_DIR}/../../include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../DebugMacro/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../Algorithm/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../Support/include
    )

target_link_libraries(
    QDELib
    ${PYTHON_LIBRARIES}
    # Qt
    ${Qt5Core_LIBRARIES}
    ${Qt5Xml_LIBRARIES}
    ${Qt5Gui_LIBRARIES}
    # QDELib
    ${CMAKE_CURRENT_SOURCE_DIR}/../../build/libqdelib.a
    )
