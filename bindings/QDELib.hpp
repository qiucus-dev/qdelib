#ifndef QDE_QSLINK_WRAPPER
#define QDE_QSLINK_WRAPPER

#include <memory>
#include <qdelib/qslink.hpp>
#include <qdelib/qsnote.hpp>
#include <qdelib/qstodo.hpp>
#include <string>
#include <vector>

class QSLink
{
  public:
    void                     setURL(std::string);
    std::string              getURL() const;
    void                     setLinkTags(std::vector<std::string> tags);
    std::vector<std::string> getLinkTags();
    void                     addLinkTag(std::string tag);

    bool openFile(std::string absPath, bool appendExtension = false);

    bool saveFile(
        std::string absPath,
        bool        appendExtension = false,
        bool        autoCreatePath  = true);


    void        setMetaDescription(std::string description);
    std::string getMetaDescription() const;
    void        setMetaNote(std::string note);
    std::string getMetaNote() const;
    void        setContentText(std::string note);
    std::string getContentText() const;


  private:
    qde::QSLink impl;
};

class QSNote
{
  public:
    void setMetaDescription(std::string description);
    void setMetaNote(std::string note);
    void setNoteText(std::string text);
    void setNoteTags(std::vector<std::string> tags);
    void addNoteTag(std::string tag);
    void setMetaTags(std::vector<std::string> tags);
    void addMetaTag(std::string tag);

    bool openFile(std::string absPath, bool appendExtension = false);

    bool saveFile(
        std::string absPath,
        bool        appendExtension = false,
        bool        autoCreatePath  = true);

  private:
    qde::QSNote impl;
};

class QSTodo
{
  public:
    /// Current completion status of the task
    enum class Status
    {
        None,        ///< Undefined status
        NeedsAction, ///< Things need to be done
        InProgress,  ///< Task is currenly being completed
        Completed,   ///< Task has been completed
        Cancelled,   ///< Task has been cancelled
        Postponed,   ///< Task deadline has been moved into the future
        Deleted,     ///< task has been deleted
        Nuked,       ///< nuked
        Stalled,     ///< Stalled
    };

    /// Priority of the task
    enum class Priority : size_t
    {
        None,       ///< Undefined priority
        NoPriority, ///< Task does not have any priority
        Low,        ///< Low importance
        Medium,     ///< Medium importance
        High,       ///< High importance
        Critical,   ///< Critical importance
        Blocking,   ///< No other tasks can be completed before this
        /*!
         * Priority does not matter now because task is currently being
         * completed
         */
        CurrentlyWorking,
        /*!
         * Priority does not matter now because task involves
         * organizational activities and can be performed during period of
         * time of any length
         */
        Organization,
        /// Priority does not matter now because this will be done after
        /// undefined preiod of time
        Later
    };

    /// Estimated amount of time that will be spent to complete this task
    enum class EstimatedTime : size_t
    {
        None,        ///< Undefined amount of time
        _5_10_min,   ///< 5 to 10 minutes
        _10_30_min,  ///< 10 to 30 minutes
        _1_2_hours,  ///< 1 to 2 hours
        _3_6_hours,  ///< 3 to 6 hours
        _1_2_days,   ///< 1 to 2 days
        _5_6_days,   ///< 5 to 6 days
        _1_3_weeks,  ///< 1 to 3 weeks
        _1_2_months, ///< 1 to 2 months
        _5_6_months, ///< 5 to 6 months
        _forever     ///< Unlimited amount of time
    };

    /// How necessary given task is
    enum class Necessity
    {
        None,       ///< Undefined necessity
        Useless,    ///< Task's completion is meaningless
        Useful,     ///< Task's completion has some value
        VeryUseful, ///< Task's completion has a lot of value
        Important,  ///< Task's completion is enforced by external actions
        VeryImportant, ///< Task's completion is strictly enforced by
                       ///< external actions
    };

    /// Chance of this task actually being completed in time (without
    /// moving it in the future)
    enum class CompletionLikelihood
    {
        None,
        Small,
        Medium,
        High,
        Guaranteed
    };

    QSTodo();

  public:
    void setTodoTags(std::vector<std::string> tags);
    void setTaskDescription(std::string description);
    void setCompletionLikelihood(CompletionLikelihood value);
    void setStatus(Status value);
    void setEstimatedTime(EstimatedTime value);
    void setNecessity(Necessity value);
    void setPriority(Priority value);
    void addTodoTag(std::string tag);
    void saveFile(std::string absPath);
    void appendNestedTodo(QSTodo todo);
    void setCustomData(std::string identifier, std::string content);

  private:
    std::shared_ptr<qde::QSTodo> impl;
};


#endif // QDE_QSLINK_WRAPPER
