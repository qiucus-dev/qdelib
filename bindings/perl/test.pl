#! /usr/bin/perl
use lib '.';
use QDELib;
use strict;
use warnings;
use v5.20;
use Cwd;

my $script_dir = getcwd;

my $link = QDELib::QSLink->new();
$link->DESTROY();

my $note = QDELib::QSNote->new();
$note->DESTROY();

my $todo = QDELib::QSTodo->new();

$todo->addTodoTag("test");

# Imperative and OOP style
QDELib::QSTodo::setStatus($todo, $QDELib::QSTodo::Status_InProgress);
$todo->setStatus($QDELib::QSTodo::Status_InProgress);

$todo->saveFile("$script_dir/test.qstodo");

$todo->DESTROY();
