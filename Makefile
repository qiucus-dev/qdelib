msg := $(shell  bash -c "((colecho > /dev/null && echo 'colecho -v ') \
2> /dev/null || echo 'echo -e \"\\033[91m[========]\033[0m: \"-- ')")

none:
	@$(msg) -e0 "You need to specify target explicitly"


##= Global

rebuild-all: clean-all build-all
	@$(msg) "rebuild-all done"

clean-all: clean-tests clean-library
	@$(msg) "clean-all done"

build-all: build-library build-tests
	@$(msg) "build-all done"

run-all: run-tests
 @$(msg) "run-all done"


##= Tests

build-tests:
	@$(msg) -i0 "Compiling tests"
	@{ cd tests ; mkdir -p build ; cd build ; cmake .. ; make -j10 -s ; }

clean-tests:
	@$(msg) -w1 "Cleaning tests"
	@{ cd tests ; rm -rf build ; rm -rf tmp ; rm -f *make_* ; }

run-tests:
	@$(msg) -i0 "Running tests"
	@{ cd tests/build ; main ; }

##= Library

build-library:
	@$(msg) -i3 "Compiling library"
	@{ mkdir -p build ; cd build ; qmake .. ; make -j12 -s ; }
	@mkdir -p lib
	@cp build/libqdelib.a lib/
	@$(msg) -i0 "Done building library"

clean-library:
	@$(msg) -w1 "Cleaning up library"
	@{ rm -rf build; rm -rf lib; }
