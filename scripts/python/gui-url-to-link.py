#! /usr/bin/env python
import sys
import os
import json
import codecs
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtWebEngineWidgets import *
from QDELib import QSLink


def get_new_link_name(note_folder: str) -> str:
    """
    Generate new name for the note that is not present in the folder.

    Names are are generated as untitled{}.qslink where {} is replaced
    with number and tested until new name is found that does not exist
    """
    idx: int = 0

    res = "untitled{}.qslink".format(idx)
    out = "{}/{}".format(note_folder, res)
    while os.path.exists(out):
        res = "untitled{}.qslink".format(idx)
        out = "{}/{}".format(note_folder, res)
        idx += 1

    return res


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.left = 1000
        self.top = 1000
        self.width = 640
        self.height = 480
        self.title = "text2note"
        self.url_edit = QLineEdit()
        self.url_tags_edit = QLineEdit()
        self.relative_save_path = QLabel()
        self.save_dir = QLineEdit()
        self.final_path = QLabel()
        self.save_dir_preview = QListView()
        self.save_dir_model = QFileSystemModel()
        self.url_preview = QWebEngineView()

        self.initUI()
        self.updateUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        save_text = QPushButton("Save new")
        save_text.clicked.connect(self.save_new_link)
        self.save_dir.returnPressed.connect(self.updateUI)
        self.url_edit.returnPressed.connect(self.updateUI)

        self.save_dir_model.setFilter(QDir.NoDotAndDotDot | QDir.Files)
        self.save_dir_preview.setModel(self.save_dir_model)

        layout = QGridLayout()
        self.setLayout(layout)

        layout.addWidget(self.url_preview, 1,1,1,2)

        url_edit_label = QLabel()
        url_edit_label.setText("Url")
        layout.addWidget(url_edit_label, 2, 1, 1, 2)
        layout.addWidget(self.url_edit, 2, 2, 1, 2)

        url_tags_label = QLabel()
        url_tags_label.setText("Tags")
        layout.addWidget(url_tags_label, 3, 1, 1, 2)
        layout.addWidget(self.url_tags_edit, 3, 2, 1, 2)

        layout.addWidget(save_text, 4, 1)
        layout.addWidget(self.relative_save_path, 4, 2)
        layout.setColumnStretch(2, 3)
        layout.setRowStretch(1,4)

        save_dir_label = QLabel()
        save_dir_label.setText("Save dir")
        layout.addWidget(self.save_dir, 5, 2)
        layout.addWidget(save_dir_label, 5, 1)
        layout.addWidget(self.final_path, 6, 1, 1, 2)

        layout.addWidget(self.save_dir_preview, 1, 3, 6, 1)

        self.relative_save_path.setText(self.get_new_link_name())
        self.save_dir.setText(os.getcwd())

        self.show()

    def get_save_folder(self) -> str:
        return self.save_dir.text()

    def get_new_link_name(self) -> str:
        return get_new_link_name(self.get_save_folder())

    def updateUI(self) -> None:
        self.save_dir_model.setRootPath(self.get_save_folder())
        self.save_dir_preview.setRootIndex(
            self.save_dir_model.index(self.get_save_folder()))
        self.final_path.setText(
            os.path.join(self.get_save_folder(), self.get_new_link_name()))
        self.relative_save_path.setText(self.get_new_link_name())
        self.url_preview.load(self.url_edit.text())

    @Slot()
    def save_new_link(self) -> None:
        abs_path: str = os.path.join(self.get_save_folder(),
                                     self.get_new_link_name())

        tags = [x.strip() for x in self.url_tags_edit.text().split(',')]
        link = QSLink()
        print(tags)
        print(self.url_edit.text())
        link.setURL(self.url_edit.text())
        link.setLinkTags(tags)
        link.saveFile(abs_path)

        self.url_edit.clear()
        self.url_tags_edit.clear()

        self.updateUI()


    def set_save_folder(self, target_dir: str) -> None:
        self.save_dir.setText(target_dir)
        self.updateUI()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    ex.set_save_folder(os.path.join(os.getcwd(), "tmp.url2link_out"))
    sys.exit(app.exec_())
