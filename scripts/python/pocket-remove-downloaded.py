#!/usr/bin/env python

import requests
import json
from common import *
from pprint import pprint

keys = json.load(open("pocket-tokens.json"))
link_list = json.load(open("tmp.pocket_out/raw/url_get.json"))

stats = {}
stats["count"] = 0

actions = []

for idx, link in link_list["list"].items():
    actions.append({
        'item_id': link["item_id"],  #
        "action": "delete"
    })

print(red("Removing all items from pocket"))

res = requests.post(
    'https://getpocket.com/v3/send',
    data={
        'consumer_key': keys["consumer_key"],
        'access_token': keys["access_token"],
        'actions': json.dumps(actions)
    })

pprint(res.headers)

printReturn(res.status_code)
