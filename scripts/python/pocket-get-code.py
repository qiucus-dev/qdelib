#!/usr/bin/env python

import requests
from fabulous.color import bold, green, red
import json
from common import *

keys = json.load(open("keys.json"))["pocket"]

pocket_api = requests.post(
    'https://getpocket.com/v3/oauth/request',
    data={
        'consumer_key': keys["consumer_key"],
        'redirect_uri': keys["redirect"]
    })

printReturn(pocket_api.status_code)

print(pocket_api.text)

code = pocket_api.text[len("code="):]

auth_keys = {}

auth_keys["consumer_key"] = keys["consumer_key"]
auth_keys["code"] = code

print("Acquired acces tokens")
print("    Code        ", green(auth_keys["code"]))
print("    Consumer key", green(auth_keys["consumer_key"]))
print(
    "To validate tokens please head over to\n",
    "https://getpocket.com/auth/authorize?request_token={}&redirect_uri={}".
    format(code, keys["redirect"]))

with open("pocket-tokens.json", "w") as tokens:
    json.dump(auth_keys, tokens)

print("Tokens were saved to", green("pocket-tokens.json"))
print("After you have authorized in url given above run",
      bold("pocket-get-access-token.py"))
