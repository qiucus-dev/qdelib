#!/usr/bin/env python

from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from typing import List, MutableMapping
from QDELib import QSTodo
import os

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/tasks.readonly'


class Task:
    def __init__(self):
        self.id = None
        self.has_parent = False
        self.google_task = None
        self.task_list = None
        self.parent_id = None
        self.child_tasks: MutableMapping['str', Task] = {}

    def get_description(self) -> str:
        res: str = self.google_task['title'] + "\n\n"

        if 'notes' in self.google_task:
            res = res + self.google_task['notes']

        return res


def fetch_tasks() -> MutableMapping[str, Task]:
    """Shows basic usage of the Tasks API.
    Prints the title and ID of the first 10 task lists.
    """
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('google-credentials.json',
                                              SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('tasks', 'v1', http=creds.authorize(Http()))

    # Call the Tasks API
    results = service.tasklists().list().execute()
    lists = results.get('items', [])

    task_dict: MutableMapping[str, Task] = {}

    for list in lists:
        tasks = service.tasks().list(
            tasklist=list['id'], maxResults=10).execute().get('items', [])
        for task in tasks:
            task_wrap = Task()
            task_wrap.google_task = task
            task_wrap.task_list = list['title']
            if 'parent' in task:
                task_wrap.has_parent = True
                task_wrap.parent_id = task['id']

            task_wrap.id = task['id']

            task_dict[task_wrap.id] = task_wrap

    return task_dict


def arrange_tasks(task_list: MutableMapping[str, Task]) -> List[Task]:
    result: MutableMapping[str, Task] = {}
    # List of task id's that were moved to a lower level
    reparented: List[str] = []
    for task_id, task in task_list.items():
        if task.has_parent:
            # Because this is first iteration there is only two cases
            # This is toplevel task or it's parent is in the list. If this is toplevel
            # task it has no parent. If this is not toplevel task we find it's parent
            # and add our task to the 'child' list. Because all tasks remain
            # in the list until the end of the cycle there will be no task that
            # has a parent but haven't been inserted into it's child list.

            # Adding task as child one
            result[task_id] = Task()
            result[task_id].child_tasks[task_id] = task
            reparented.append(task_id)  # Add id for later deletion

    for id in reparented:
        result.pop(id)

    res: List[Task] = []

    for task_id, task in task_list.items():
        res.append(task)

    return res

def fold_nested(task: Task, todo:QSTodo) -> None:
    for id, child in task.child_tasks.items():
        nested: QSTodo = QSTodo()
        nested.setTaskDescription(task.get_description())
        fold_nested(child, todo)
        todo.appendNestedTodo(nested)

if __name__ == '__main__':
    task_list: List[Task] = arrange_tasks(fetch_tasks())

    for task in task_list:
        qstodo = QSTodo()
        qstodo.setTaskDescription(task.get_description())

        path = os.path.join(os.getcwd(), "saved-gtask", task.task_list,
                            task.google_task['id'][:20] + ".qstodo")

        fold_nested(task, qstodo)
        qstodo.saveFile(path)
