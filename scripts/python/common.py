from fabulous.color import bold, green, red
from transliterate import translit
from langdetect import detect as detectLanguage


def printReturn(code):
    if code == 200:
        print(bold("Request returned ", green(code)))
    else:
        print(bold("Request returned ", red(code)))


def makeSafeName(rawName, url):
    lang = None
    fileName = None

    try:
        lang = detectLanguage(rawName)
    except Exception as e:
        lang = "en"

    if (lang == "ru"):
        fileName = translit(
            rawName, "ru", reversed=True).encode(
                "ascii", errors='ignore')
    else:
        if (not rawName):
            rawName = url

        fileName = rawName.encode("ascii", errors='ignore')

    fileName = fileName.decode()
    fileName = fileName.replace(" ", "_")
    fileName = fileName.replace(".", "_")

    return fileName
