#!/usr/bin/env python

import requests
from fabulous.color import bold, green, red
import json
import re
from common import *

keys = json.load(open("pocket-tokens.json"))

print("Authorizing")
pocket_auth = requests.post(
    'https://getpocket.com/v3/oauth/authorize',
    data={
        'consumer_key': keys["consumer_key"],
        'code': keys["code"]
    })

print("Done")
printReturn(pocket_auth.status_code)

access_token = re.search(r"access_token=(.*?)&", pocket_auth.text).group(1)
print("Access token", access_token)
keys["access_token"] = access_token

with open("pocket-tokens.json", "w") as tokens:
    json.dump(keys, tokens)

print("Tokens were saved to", green("pocket-tokens.json"))
print(
    "Now we have all requisite codes to "
    "access the Pocket account of a user",
    "\nIt is not required to run scipts from above again")
