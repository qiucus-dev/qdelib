#! /usr/bin/perl
use lib '.';
use QDELib;

use strict;
use warnings;
use Cwd;
use Switch;
use v5.20;

use feature qw(signatures);
no warnings qw(experimental::signatures);

my $script_dir = getcwd;

use JSON::MaybeXS qw(encode_json decode_json);
use Data::Dump 'dump';
use String::Random;

##= Script

my $tasks = `task export`;
$tasks = "{\"tasks\":$tasks}";
my $json_arr = ( decode_json $tasks)->{tasks};

my $string_gen = String::Random->new;

for my $task (@$json_arr) {
    my $path =
        "$script_dir/saved/$task->{id}-"
      . time() . "_"
      . $string_gen->randregex('\d\d\d\d\d\d\d\d')
      . ".qstodo";

    my $todo = QDELib::QSTodo->new();

    $todo->setTaskDescription( $task->{description} );
    $todo->saveFile("$path");

    $todo->DESTROY();
}
